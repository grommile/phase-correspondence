Phase Correspondence is a seven-day roguelike created pursuant to the 2016
Seven-Day Roguelike Challenge.

## Installation

To build the game executable, run `make all` in the base directory of the
source tree.

Optionally, run `make install` as root to create a system-wide installation,
though this isn't necessary as the game doesn't load any static data at
run time.

## Code re-use

Phase Correspondence is derived from the source code of my previous project, Kebich.

## Dependencies

* A version of GCC 4.x recent enough to recognize `-std=gnu11`
* A non-antediluvian version of GNU Make
* ncurses 5.x or later

## An apology to people with colour vision impairments

I would like to acknowledge in advance that *Phase Correspondence*'s planned UI design makes heavy use of colour distinction and is unlikely to work well for people with colour vision impairments. I hope to include user interface elements which make the information conveyed via colour contrasts available via other means e.g. textual descriptions.

