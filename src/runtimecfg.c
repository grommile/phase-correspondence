/*! \file runtimecfg.c
 * \brief Run-time configuration loader
 */

/* Copyright © 2005, 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "central.h"
#include "util.h"
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef GETTIMEOFDAY_HAS_REAL_USEC
#include <sys/time.h>
#endif
#include <time.h>
#include <locale.h>
#include <errno.h>
#include <string.h>

static int datadir_fd;
static int configdir_fd;
struct grommile_kvstore *config_store;
enum
{
    MAX_CFG_LINE_LEN = 1024
};

static int read_kv_pairs(int fd, struct grommile_kvstore *store)
{
    FILE *fp = fdopen(fd, "r");
    if (fp == NULL)
    {
        return -1;
    }
    char linebuf[MAX_CFG_LINE_LEN + 2];
    char keybuf[MAX_CFG_LINE_LEN + 2];
    char valbuf[MAX_CFG_LINE_LEN + 2];
    char *s;
    char *eol;
    int i;
    do
    {
        s = fgets(linebuf, MAX_CFG_LINE_LEN + 2, fp);
        eol = strchr(s, '\n');
        if (eol == NULL)
        {
            NOTICE("%s", "Configuration file contains a line more than 1024 bytes long");
            return -1;
        }
        if (s == linebuf)
        {
            s += strspn(s, " \t");
            if (s[0] == '\n')
            {
                // comment line
                continue;
            }
            i = sscanf(s, "%512[^ =\t\n] = %512[^ =\t\n]", keybuf, valbuf);
            if (i == 2)
            {
                grommile_store_kv(store, keybuf, valbuf);
            }
            else
            {
                NOTICE("%s", "Non-comment line in config file fails to specify a variable.");
            }
        }
    } while (!(feof(fp) || ferror(fp)));
    fclose(fp);
    return 0;
}

void load_config(void)
{
    int i;
    i = grommile_setup_dirs("blackswordsonics.com", "phase-correspondence", &datadir_fd, &configdir_fd);
    if (i == -1)
    {
        NOTICE("Couldn't set up per-user config and data directories: %s", strerror(errno));
    }
    config_store = grommile_create_kvstore(1);
    if (!config_store)
    {
        FATAL("Couldn't create configuration key/value store: %s", strerror(errno));
    }
    int fd = openat(configdir_fd, "phase-correspondence.cfg", O_RDONLY, 0);
    if (fd != -1)
    {
        i = read_kv_pairs(fd, config_store);
    }
}

char const *get_config(char const *key)
{
    return grommile_fetch_kv(config_store, key);
}

int get_config_as_integer(char const *key, int32_t *destptr)
{
    char const *valptr = grommile_fetch_kv(config_store, key);
    if (!valptr)
    {
        return -1;
    }
    int32_t tmp;
    errno = 0;
    tmp = strtol(valptr, NULL, 0);
    if (errno != 0)
    {
        return -1;
    }
    *destptr = tmp;
    return 0;
}

