/*! \file map.c
 * \brief map handling
 */

/* Copyright © 2005, 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "central.h"
#include "objfun.h"
#include "pobjfun.h"
#include "monfun.h"
#include "pmonfun.h"
#include "mapfun.h"
#include "ufun.h"

#include <string.h>

// the hidden *actual* map structure
static Map real_map;
// the pointer everything else uses to refer to the map structure
Map *current_map = &real_map;

/*! \brief pool index for objects on the current level */
int32_t floor_obj_pool;
/*! \brief pool index for monsters on the current level */
int32_t floor_mon_pool;

int dun_max_y = MAP_SIZE - 1;
int dun_max_x = MAP_SIZE - 1;
int dun_min_y = 0;
int dun_min_x = 0;
/*! \brief connectivity tracking for rooms */
static int roomlinkage[MAX_ROOMS][MAX_ROOMS];

/*! \brief boundaries of rooms on current level */
Roombound roombounds[MAX_ROOMS];
int stairs_room = NO_ROOM;
static int segsused[MAX_ROOMS];

static Passfail get_levgen_mon_floor(int depth, int *y, int *x);
static void add_random_room(int8_t map_num, int8_t layer, int yseg, int xseg);
static bool rooms_adjacent(int r1, int r2);
static Passfail link_rooms(int r1, int r2);
static void put_stairs(int depth);

void room_reset(int8_t map_num)
{
    int i, j;
    int8_t layer;
    for (i = 0; i < MAX_ROOMS; ++i)
    {
        roombounds[0].top_y = roombounds[0].bot_y = roombounds[0].left_x = roombounds[0].right_x = 0;
        for (j = 0; j < MAX_ROOMS; ++j)
        {
            roomlinkage[i][j] = (i == j) ? Roomlink_self : Roomlink_none;
        }
        segsused[i] = 0;
    }
    stairs_room = NO_ROOM;
    for (layer = 0; layer < MAP_LAYERS; ++layer)
    {
        for (i = 0; i < MAP_SIZE; ++i)
        {
            for (j = 0; j < MAP_SIZE; ++j)
            {
                set_mon_at(map_num, layer, i, j, NO_MON);
                set_obj_at(map_num, layer, i, j, NO_OBJ);
                set_terrain_at(map_num, layer, i, j, WALL);
                overwrite_map_flags_at(map_num, layer, i, j, 0);
                set_region_at(map_num, layer, i, j, NO_ROOM);
            }
        }
    }
}

static void add_random_room(int8_t map_num, int8_t layer, int yseg, int xseg)
{
    int roomidx = (yseg * ROOMS_PER_ROW) + xseg;
    int ycen, xcen;
    int y1, y2, x1, x2;
    int y, x;
    ycen = (MAP_SIZE - 2) / (ROOMS_PER_ROW * 2) + yseg * ((MAP_SIZE - 2) / ROOMS_PER_ROW);
    xcen = (MAP_SIZE - 2) / (ROOMS_PER_COLUMN * 2) + xseg * ((MAP_SIZE - 2) / ROOMS_PER_COLUMN);
    y1 = ycen - one_die(2) - 1;
    x1 = xcen - one_die(2) - 1;
    y2 = ycen + one_die(2) + 1;
    x2 = xcen + one_die(2) + 1;
    for (y = y1 + 1; y < y2; y++)
    {
        for (x = x1 + 1; x < x2; x++)
        {
            set_terrain_at(map_num, layer, y, x, FLOOR);
            set_region_at(map_num, layer, y, x, roomidx);
        }
    }
    for (y = y1; y <= y2; y++)
    {
        set_region_at(map_num, layer, y, x1, roomidx);
        set_region_at(map_num, layer, y, x2, roomidx);
    }
    for (x = x1; x <= x2; x++)
    {
        set_region_at(map_num, layer, y1, x, roomidx);
        set_region_at(map_num, layer, y2, x, roomidx);
    }
    roombounds[roomidx].top_y = y1;
    roombounds[roomidx].bot_y = y2;
    roombounds[roomidx].left_x = x1;
    roombounds[roomidx].right_x = x2;
    segsused[yseg * ROOMS_PER_ROW + xseg] = true;
}

/*! \brief Link two rooms together
 *
 * \param r1 Room number of first room
 * \param r2 Room number of second room
 */
static Passfail link_rooms(int r1, int r2)
{
    int i;
    int y, x;
    int y1, y2, y3, y4;
    int x1, x2, x3, x4;
    /* Bounds check */
    if ((r1 >= MAX_ROOMS) || (r1 < 0) || (r2 >= MAX_ROOMS) || (r2 < 0))
    {
        return PF_fail;
    }
    /* Don't link the same pair of rooms twice */
    if ((roomlinkage[r1][r2] == Roomlink_direct) ||
        (roomlinkage[r1][r2] == Roomlink_direct))
    {
        return PF_fail;
    }
    /* Don't link non-adjacent rooms this way */
    if (!rooms_adjacent(r1, r2))
    {
        return PF_fail;
    }
    /* Update the linkage matrix. */
    roomlinkage[r1][r2] = Roomlink_direct;
    roomlinkage[r2][r1] = Roomlink_direct;
    for (i = 0; i < MAX_ROOMS; i++)
    {
        if ((i == r1) || (i == r2))
        {
            continue;
        }
        if ((roomlinkage[r1][i] != Roomlink_none) &&
            (roomlinkage[r2][i] != Roomlink_direct))
        {
            roomlinkage[r2][i] = Roomlink_indirect;
            roomlinkage[i][r2] = Roomlink_indirect;
        }
        if ((roomlinkage[r2][i] != Roomlink_none) &&
            (roomlinkage[r1][i] != Roomlink_direct))
        {
            roomlinkage[r1][i] = Roomlink_indirect;
            roomlinkage[i][r1] = Roomlink_indirect;
        }
    }
    y1 = roombounds[r1].top_y;
    y2 = roombounds[r2].top_y;
    y3 = roombounds[r1].bot_y;
    y4 = roombounds[r2].bot_y;
    x1 = roombounds[r1].left_x;
    x2 = roombounds[r2].left_x;
    x3 = roombounds[r1].right_x;
    x4 = roombounds[r2].right_x;
    /* Now generate the corridor. */
    if ((r1 % 3) == (r2 % 3))
    {
        /* same xseg; north-south linkage */
        if (x4 < x3)
        {
            x3 = x4;
        }
        if (x2 > x1)
        {
            x1 = x2;
        }
        x = exclusive_flat(x1, x3);
        if (y3 < y2)
        {
            /* go south from r1 */
            SET_TERR_AT(y3,x,DOOR);
            SET_TERR_AT(y2,x,DOOR);
            for (y = y3 + 1; y < y2; y++)
            {
                SET_TERR_AT(y,x,FLOOR);
            }
        }
        else if (y4 < y1)
        {
            /* go south from r2 */
            SET_TERR_AT(y4,x,DOOR);
            SET_TERR_AT(y1,x,DOOR);
            for (y = y4 + 1; y < y1; y++)
            {
                SET_TERR_AT(y,x,FLOOR);
            }
        }
    }
    else
    {
        /* same yseg; east-west linkage */
        if (y4 < y3)
        {
            y3 = y4;
        }
        if (y2 > y1)
        {
            y1 = y2;
        }
        y = exclusive_flat(y1, y3);
        if (x3 < x2)
        {
            /* go south from r1 */
            SET_TERR_AT(y,x3,DOOR);
            SET_TERR_AT(y,x2,DOOR);
            for (x = x3 + 1; x < x2; x++)
            {
                SET_TERR_AT(y,x,FLOOR);
            }
        }
        else if (x4 < x1)
        {
            /* go south from r2 */
            SET_TERR_AT(y,x4,DOOR);
            SET_TERR_AT(y,x1,DOOR);
            for (x = x4 + 1; x < x1; x++)
            {
                SET_TERR_AT(y,x,FLOOR);
            }
        }
    }
    return PF_pass;
}

void leave_level(int depth MYUNUSED)
{
    int i, j;
    /* Clear the grids */
    for (i = 0; i < MAP_SIZE; ++i)
    {
        for (j = 0; j < MAP_SIZE; ++j)
        {
            SET_OBJ_AT(i,j,NO_OBJ);
            SET_MON_AT(i,j,NO_MON);
            SET_TERR_AT(i,j,WALL);
            SET_FLAGS_AT(i,j,0);
            SET_REGION_AT(i,j,NO_ROOM);
        }
    }
    /* Throw away each monster */
    for (i = 0; i < MAX_MONS_IN_PLAY; ++i)
    {
        monsters[i].used = false;
    }
    /* and each object not carried by the player */
    for (i = 0; i < MAX_OBJS_IN_PLAY; ++i)
    {
        if (!objects[i].with_you)
        {
            objects[i].used = false;
        }
    }
    status_updated = true;
    map_updated = true;
}

void make_new_level(int depth)
{
    room_reset(depth);
    build_level(depth);
    populate_level(depth);
    inject_player(depth);
}

void put_stairs(int depth MYUNUSED)
{
    int y;
    int x;
    stairs_room = zero_die(MAX_ROOMS);
    y = exclusive_flat(roombounds[stairs_room].top_y, roombounds[stairs_room].bot_y);
    x = exclusive_flat(roombounds[stairs_room].left_x, roombounds[stairs_room].right_x);
    SET_TERR_AT(y,x,STAIRS);
}

/*! \brief checks if two rooms are adjacent
 *
 * Because I'm sure someone will ask: rooms are not *adjacent to* themselves,
 * they *are* themselves.
 */
static bool rooms_adjacent(int r1, int r2)
{
    if (r1 == r2)
    {
        return false;
    }
    if (room_column(r1) == room_column(r2))
    {
        int delta = room_row(r1) - room_row(r2);
        return (delta == 1) || (delta == -1);
    }
    if (room_row(r1) == room_row(r2))
    {
        int delta = room_column(r1) - room_column(r2);
        return (delta == 1) || (delta == -1);
    }
    return false;
}

int edge_rooms[4] = { 1, 3, 5, 7 };
int corners[4][2] = { { 0, 2 }, { 0, 6 }, { 2, 8 }, { 6, 8 } };
void build_level(int depth)
{
    int i;
    /* Snapshot the running RNG state, so that we can rebuild the map from
     * the saved RNG state at game reload. */
    save_rng_state();
    /* Add rooms */
    for (i = 0; i < MAX_ROOMS; i++)
    {
        add_random_room(depth, 0, i / ROOMS_PER_ROW, i % ROOMS_PER_ROW);
    }
    /* Add corridors */
    /* Link the centre room to an edge room. */
    link_rooms(4, edge_rooms[zero_die(4)]);
    /* And to another; if we're already linked, don't bother. */
    i = zero_die(4);
    if (roomlinkage[4][edge_rooms[i]] == Roomlink_none)
    {
        link_rooms(4, edge_rooms[i]);
    }
    /* Link each edge room to one of its corner rooms. */
    for (i = 0; i < 4; i++)
    {
        link_rooms(edge_rooms[i], corners[i][zero_die(2)]);
    }
    /* At this point, 1-2 edge rooms and their attached corner rooms
     * have linkage to the centre. */
    /* Link each edge room to its unlinked corner if it is not 2-linked
     * to the centre. */
    for (i = 0; i < 4; i++)
    {
        if (!roomlinkage[4][edge_rooms[i]])
        {
            if (roomlinkage[edge_rooms[i]][corners[i][0]])
            {
                link_rooms(edge_rooms[i], corners[i][1]);
            }
            else
            {
                link_rooms(edge_rooms[i], corners[i][0]);
            }
        }

    }
    /* Link each corner room to its unlinked edge if that edge is not
     * 2-linked to the centre.  If we still haven't got centre
     * connectivity for the edge room, connect the edge to the centre. */
    for (i = 0; i < 4; i++)
    {
        if (!roomlinkage[4][edge_rooms[i]])
        {
            if (!roomlinkage[edge_rooms[i]][corners[i][0]])
            {
                link_rooms(edge_rooms[i], corners[i][0]);
            }
            if (!roomlinkage[edge_rooms[i]][corners[i][1]])
            {
                link_rooms(edge_rooms[i], corners[i][1]);
            }
        }
        if (!roomlinkage[4][edge_rooms[i]])
        {
            link_rooms(edge_rooms[i], 4);
        }
    }
    /* Just for safety's sake: Now we know all edges are attached,
     * make sure all the corners are. (Previously, it was possible
     * for them not to be. I know, because I met such a level :) */
    for (i = 3; i >= 0; i--)
    {
        if (!roomlinkage[4][corners[i][0]])
        {
            link_rooms(edge_rooms[i], corners[i][0]);
        }
        if (!roomlinkage[4][corners[i][1]])
        {
            link_rooms(edge_rooms[i], corners[i][1]);
        }
    }
    /* Add the stairs */
    put_stairs(depth);
}

int get_room_y(int room)
{
    return exclusive_flat(roombounds[room].top_y, roombounds[room].bot_y);
}

int get_room_x(int room)
{
    return exclusive_flat(roombounds[room].left_x, roombounds[room].right_x);
}

Passfail get_levgen_mon_floor(int depth MYUNUSED, int *y, int *x)
{
    /* Get a vacant floor cell that isn't in the treasure zoo. */
    int room_try;
    int cell_try;
    int ty = *y;
    int tx = *x;
    int room;
    for (room_try = 0; room_try < (MAX_ROOMS * 2); room_try++)
    {
        room = zero_die(MAX_ROOMS);
        for (cell_try = 0; cell_try < 200; cell_try++)
        {
            ty = get_room_y(room);
            tx = get_room_x(room);
            if ((TERR_AT(ty,tx) != FLOOR) ||
                (MON_AT(ty,tx) != NO_MON))
            {
                ty = NO_POS;
                tx = NO_POS;
                continue;
            }
            break;
        }
        break;
    }
    if (ty == NO_POS)
    {
        return PF_fail;
    }
    *y = ty;
    *x = tx;
    return PF_pass;
}

void populate_level(int depth)
{
    int i;
    Passfail pf;
    int y, x;
    /* Generate some random monsters */
    for (i = 0; i < 10; i++)
    {
        pf = get_levgen_mon_floor(depth, &y, &x);
        if (pf == PF_fail)
        {
            continue;
        }
        create_mon(NO_PMON, depth, y, x);
    }
    /* Generate some random treasure */
    for (i = 0; i < (3 + depth); i++)
    {
        pf = get_levgen_mon_floor(depth, &y, &x);
        if (pf == PF_fail)
        {
            continue;
        }
        create_obj(NO_POBJ, 1, false, depth, y, x);
    }
}

void inject_player(int depth MYUNUSED)
{
    int i;
    int room_try;
    int cell_try;
    for (room_try = 0; room_try < (MAX_ROOMS * 2); room_try++)
    {
        i = zero_die(MAX_ROOMS);
        if (i == stairs_room)
        {
            continue;
        }
        for (cell_try = 0; cell_try < 200; cell_try++)
        {
            u.y = exclusive_flat(roombounds[i].top_y, roombounds[i].bot_y);
            u.x = exclusive_flat(roombounds[i].left_x, roombounds[i].right_x);
            if (MON_AT(u.y,u.x) != NO_MON)
            {
                continue;
            }
            break;
        }
        break;
    }
    reloc_player(u.y, u.x);
}

/* map.c */
