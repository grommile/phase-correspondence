/*! \file serdes.c
 * \brief Serialization/deserialization
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "central.h"
#include "gameio.h"
#include "objfun.h"
#include "pobjfun.h"
#include "monfun.h"
#include "pmonfun.h"
#include "mapfun.h"
#include "ufun.h"
#include "prng.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef GETTIMEOFDAY_HAS_REAL_USEC
#include <sys/time.h>
#endif
#include <time.h>
#include <locale.h>

static void ser_map(FILE *fp);
static void ser_Player(FILE *fp, Player const *pptr);
static void ser_Mon(FILE *fp, Mon const *mptr);
static void ser_Obj(FILE *fp, Obj const *optr);
static void ser_Permon_stat(FILE *fp, Permon_stat const *pmsptr);
static void ser_Permobj_stat(FILE *fp, Permobj_stat const *posptr);
static void ser_int8(FILE *fp, int8_t i) MYUNUSED;
static void ser_int16(FILE *fp, int16_t i) MYUNUSED;
static void ser_int32(FILE *fp, int32_t i);
static void deser_map(FILE *fp);
static void deser_Player(FILE *fp, Player *pptr);
static void deser_Mon(FILE *fp, Mon *mptr);
static void deser_Obj(FILE *fp, Obj *optr);
static void deser_Permon_stat(FILE *fp, Permon_stat *pmsptr);
static void deser_Permobj_stat(FILE *fp, Permobj_stat *posptr);
static void deser_int8(FILE *fp, int8_t *i) MYUNUSED;
static void deser_int16(FILE *fp, int16_t *i) MYUNUSED;
static void deser_int32(FILE *fp, int32_t *i);

#define WORKBUF_DECL(size) uint8_t work_buffer[(size)]; uint8_t *wbuf_ptr MYUNUSED = work_buffer
#define WORKBUF_PUSH(x, size) do { memcpy(wbuf_ptr, (x), (size)); wbuf_ptr += (size); } while (0)
#define WORKBUF_PUSH_8(x) do { *(wbuf_ptr++) = x; } while (0)
#define WORKBUF_PUSH_16(x) do { tmpi16 = htobe16((x)); WORKBUF_PUSH(&tmpi16, sizeof tmpi16); } while (0)
#define WORKBUF_PUSH_32(x) do { tmpi32 = htobe32((x)); WORKBUF_PUSH(&tmpi32, sizeof tmpi32); } while (0)
#define WORKBUF_PUSH_16ARR(x, n) do { int i; for (i = 0; i < (n); ++i) { WORKBUF_PUSH_16((x)[i]); } } while (0)
#define WORKBUF_PUSH_32ARR(x, n) do { int i; for (i = 0; i < (n); ++i) { WORKBUF_PUSH_32((x)[i]); } } while (0)
#define WORKBUF_PULL(x, size) do { memcpy((x), wbuf_ptr, (size)); wbuf_ptr += (size); } while (0)
#define WORKBUF_PULL_8(x) do { *(x) = *(wbuf_ptr++); } while (0)
#define WORKBUF_PULL_16(x) do { WORKBUF_PULL(&tmpi16, sizeof tmpi16); *(x) = be16toh(tmpi16); } while (0)
#define WORKBUF_PULL_32(x) do { WORKBUF_PULL(&tmpi32, sizeof tmpi32); *(x) = be32toh(tmpi32); } while (0)
#define WORKBUF_PULL_16ARR(x, n) do { int i; for (i = 0; i < (n); ++i) { WORKBUF_PULL_16((x) + i); } } while (0)
#define WORKBUF_PULL_32ARR(x, n) do { int i; for (i = 0; i < (n); ++i) { WORKBUF_PULL_32((x) + i); } } while (0)
#define WORKBUF_RESET() do { wbuf_ptr = work_buffer; } while (0)
#define WORKBUF_CLEAR() do { memset(work_buffer, '\0', sizeof work_buffer); } while (0)
#define TMPINTS() uint16_t tmpi16 MYUNUSED; uint32_t tmpi32 MYUNUSED
#define WRITE_WORKBUF(size) do { checked_fwrite(work_buffer, 1, (size), fp); } while (0)
#define READ_WORKBUF(size) do { checked_fread(work_buffer, 1, (size), fp); } while (0)

static void ser_map(FILE *fp)
{
    WORKBUF_DECL(MAP_SIZE * MAP_SIZE * (sizeof (uint32_t) + sizeof (uint8_t)) + (sizeof roombounds));
    TMPINTS();
    int y;
    int x;
    int rn;
    for (y = 0; y < MAP_SIZE; ++y)
    {
        for (x = 0; x < MAP_SIZE; ++x)
        {
            WORKBUF_PUSH_32(FLAGS_AT(y,x));
            WORKBUF_PUSH_8(TERR_AT(y,x));
        }
    }
    for (rn = 0; rn < MAX_ROOMS; ++rn)
    {
        WORKBUF_PUSH_32(roombounds[rn].top_y);
        WORKBUF_PUSH_32(roombounds[rn].bot_y);
        WORKBUF_PUSH_32(roombounds[rn].left_x);
        WORKBUF_PUSH_32(roombounds[rn].right_x);
    }
    WRITE_WORKBUF(sizeof work_buffer);
}

static void deser_map(FILE *fp)
{
    WORKBUF_DECL(MAP_SIZE * MAP_SIZE * (sizeof (uint32_t) + sizeof (uint8_t)) + (sizeof roombounds));
    TMPINTS();
    int y;
    int x;
    int rn;
    READ_WORKBUF(sizeof work_buffer);
    for (y = 0; y < MAP_SIZE; ++y)
    {
        for (x = 0; x < MAP_SIZE; ++x)
        {
            uint32_t f;
            uint8_t t;
            WORKBUF_PULL_32(&f);
            SET_FLAGS_AT(y,x,f);
            WORKBUF_PULL_8(&t);
            SET_TERR_AT(y,x,t);
        }
    }
    for (rn = 0; rn < MAX_ROOMS; ++rn)
    {
        WORKBUF_PULL_32(&(roombounds[rn].top_y));
        WORKBUF_PULL_32(&(roombounds[rn].bot_y));
        WORKBUF_PULL_32(&(roombounds[rn].left_x));
        WORKBUF_PULL_32(&(roombounds[rn].right_x));
    }
}

static void ser_Mon(FILE *fp, Mon const *mptr)
{
    WORKBUF_DECL(sizeof (Mon));
    TMPINTS();
    WORKBUF_PUSH_32(mptr->mon_id);
    WORKBUF_PUSH_32(mptr->y);
    WORKBUF_PUSH_32(mptr->x);
    WORKBUF_PUSH_16(mptr->hpmax);
    WORKBUF_PUSH_16(mptr->hpcur);
    WORKBUF_PUSH_16(mptr->tohit);
    WORKBUF_PUSH_16(mptr->evasion);
    WORKBUF_PUSH_16(mptr->dam);
    WORKBUF_PUSH_16(mptr->current_faction);
    WORKBUF_PUSH_8(mptr->used);
    WORKBUF_PUSH_8(mptr->awake);
    WRITE_WORKBUF(sizeof work_buffer);
}

static void deser_Mon(FILE *fp, Mon *mptr)
{
    WORKBUF_DECL(sizeof (Mon));
    TMPINTS();
    READ_WORKBUF(sizeof work_buffer);
    WORKBUF_PULL_32(&(mptr->mon_id));
    WORKBUF_PULL_32(&(mptr->y));
    WORKBUF_PULL_32(&(mptr->x));
    WORKBUF_PULL_16(&(mptr->hpmax));
    WORKBUF_PULL_16(&(mptr->hpcur));
    WORKBUF_PULL_16(&(mptr->tohit));
    WORKBUF_PULL_16(&(mptr->evasion));
    WORKBUF_PULL_16(&(mptr->dam));
    WORKBUF_PULL_16(&(mptr->current_faction));
    WORKBUF_PULL_8(&(mptr->used));
    WORKBUF_PULL_8(&(mptr->awake));
}

static void ser_Obj(FILE *fp, Obj const *optr)
{
    WORKBUF_DECL(sizeof (Obj));
    TMPINTS();
    WORKBUF_PUSH_32(optr->obj_id);
    WORKBUF_PUSH_32(optr->quan);
    WORKBUF_PUSH_32(optr->y);
    WORKBUF_PUSH_32(optr->x);
    WORKBUF_PUSH_8(optr->used);
    WORKBUF_PUSH_8(optr->with_you);
    WRITE_WORKBUF(sizeof work_buffer);
}

static void deser_Obj(FILE *fp, Obj *optr)
{
    WORKBUF_DECL(sizeof (Obj));
    TMPINTS();
    READ_WORKBUF(sizeof work_buffer);
    WORKBUF_PULL_32(&(optr->obj_id));
    WORKBUF_PULL_32(&(optr->quan));
    WORKBUF_PULL_32(&(optr->y));
    WORKBUF_PULL_32(&(optr->x));
    WORKBUF_PULL_8(&(optr->used));
    WORKBUF_PULL_8(&(optr->with_you));
}

static void ser_Permon_stat(FILE *fp, Permon_stat const *pmsptr)
{
    WORKBUF_DECL(sizeof (Permon_stat));
    TMPINTS();
    WORKBUF_PUSH_32(pmsptr->num_generated);
    WORKBUF_PUSH_32(pmsptr->killed_by_player);
    WORKBUF_PUSH_32(pmsptr->killed_by_other);
    WRITE_WORKBUF(sizeof work_buffer);
}

static void deser_Permon_stat(FILE *fp, Permon_stat *pmsptr)
{
    WORKBUF_DECL(sizeof (Permon_stat));
    TMPINTS();
    READ_WORKBUF(sizeof work_buffer);
    WORKBUF_PULL_32(&(pmsptr->num_generated));
    WORKBUF_PULL_32(&(pmsptr->killed_by_player));
    WORKBUF_PULL_32(&(pmsptr->killed_by_other));
}

static void ser_Permobj_stat(FILE *fp, Permobj_stat const *posptr)
{
    WORKBUF_DECL(sizeof (Permobj_stat));
    TMPINTS();
    WORKBUF_PUSH_32(posptr->num_generated);
    WORKBUF_PUSH_16(posptr->flavour);
    WORKBUF_PUSH_8(posptr->identified);
    WRITE_WORKBUF(sizeof work_buffer);
}

static void deser_Permobj_stat(FILE *fp, Permobj_stat *posptr)
{
    WORKBUF_DECL(sizeof (Permobj_stat));
    TMPINTS();
    READ_WORKBUF(sizeof work_buffer);
    WORKBUF_PULL_32(&(posptr->num_generated));
    WORKBUF_PULL_16(&(posptr->flavour));
    WORKBUF_PULL_8(&(posptr->identified));
}

static void ser_Player(FILE *fp, Player const *pptr)
{
    /* max hp, max shield, tohit, evasion, and shield_regen do not need
     * to be serialized, as they can be reconstructed by calling
     * recalc_derived(). It can thus be safely assumed that the work
     * buffer need not be any larger than the size of a Player. */
    WORKBUF_DECL(sizeof (Player));
    TMPINTS();
    WORKBUF_CLEAR();
    WORKBUF_PUSH(pptr->name, PLAYER_NAME_DISPLAYED);
    WORKBUF_PUSH_32(pptr->y);
    WORKBUF_PUSH_32(pptr->x);
    WORKBUF_PUSH_32(pptr->moniez);
    WORKBUF_PUSH_32ARR(pptr->inventory, INVENTORY_SIZE);
    WORKBUF_PUSH_32(pptr->invpool);
    WORKBUF_PUSH_32(pptr->melee_weap);
    WORKBUF_PUSH_32(pptr->ranged_weap);
    WORKBUF_PUSH_32(pptr->armour);
    WORKBUF_PUSH_32(pptr->plugin);
    WORKBUF_PUSH_16(pptr->hpcur);
    WORKBUF_PUSH_16(pptr->shcur);
    WORKBUF_PUSH_8(pptr->level);
    WRITE_WORKBUF(sizeof *pptr);
}

static void deser_Player(FILE *fp, Player *pptr)
{
    WORKBUF_DECL(sizeof (Player));
    TMPINTS();
    READ_WORKBUF(sizeof (Player));
    WORKBUF_PULL(pptr->name, PLAYER_NAME_DISPLAYED);
    pptr->name[PLAYER_NAME_DISPLAYED] = '\0';
    WORKBUF_PULL_32(&(pptr->y));
    WORKBUF_PULL_32(&(pptr->x));
    WORKBUF_PULL_32(&(pptr->moniez));
    WORKBUF_PULL_32ARR(pptr->inventory, INVENTORY_SIZE);
    WORKBUF_PULL_32(&(pptr->invpool));
    WORKBUF_PULL_32(&(pptr->melee_weap));
    WORKBUF_PULL_32(&(pptr->ranged_weap));
    WORKBUF_PULL_32(&(pptr->armour));
    WORKBUF_PULL_32(&(pptr->plugin));
    WORKBUF_PULL_16(&(pptr->hpcur));
    WORKBUF_PULL_16(&(pptr->shcur));
    WORKBUF_PULL_8(&(pptr->level));
}

static void ser_int8(FILE *fp, int8_t i)
{
    checked_fwrite(&i, 1, 1, fp);
}

static void deser_int8(FILE *fp, int8_t *i)
{
    checked_fread(i, 1, 1, fp);
}

static void ser_int16(FILE *fp, int16_t i)
{
    uint16_t tmp = htobe16(i);
    checked_fwrite(&tmp, 2, 1, fp);
}

static void deser_int16(FILE *fp, int16_t *i)
{
    uint16_t tmp;
    checked_fread(&tmp, 2, 1, fp);
    *i = be16toh(tmp);
}

static void ser_int32(FILE *fp, int32_t i)
{
    uint32_t tmp = htobe32(i);
    checked_fwrite(&tmp, 4, 1, fp);
}

static void deser_int32(FILE *fp, int32_t *i)
{
    uint32_t tmp;
    checked_fread(&tmp, 4, 1, fp);
    *i = be32toh(tmp);
}

static void rebuild_roomnums(void)
{
    int i;
    int32_t y;
    int32_t x;
    for (i = 0; i < MAX_ROOMS; ++i)
    {
        for (y = roombounds[i].top_y; y <= roombounds[i].bot_y; ++y)
        {
            for (x = roombounds[i].left_x; x <= roombounds[i].right_x; ++x)
            {
                SET_REGION_AT(y, x, i);
            }
        }
    }
}

static void rebuild_mapmons(void)
{
    int i;
    for (i = 0; i < MAX_MONS_IN_PLAY; i++)
    {
        if (monsters[i].used)
        {
            SET_MON_AT(monsters[i].y, monsters[i].x, i);
        }
    }
}

static void rebuild_mapobjs(void)
{
    int i;
    for (i = 0; i < MAX_OBJS_IN_PLAY; i++)
    {
        if (objects[i].used && !objects[i].with_you)
        {
            SET_OBJ_AT(objects[i].y, objects[i].x, i);
        }
    }
}

/*! \brief Checked wrapper for fopen
 *
 * Only for use in cases where "exit immediately" is an acceptable response.
 */
FILE *checked_fopen(char const *path, char const *mode)
{
    FILE *fp = fopen(path, mode);
    if (fp == NULL)
    {
        FATAL("couldn't open %s with mode %s: %s", path, mode, strerror(errno));
    }
    return fp;
}

/*! \brief Checked wrapper for fwrite
 *
 * Only for use in cases where "exit immediately" is an acceptable response.
 */
void checked_fwrite(void const *buf, size_t size, size_t nmemb, FILE *stream)
{
    size_t s = fwrite(buf, size, nmemb, stream);
    if (s != nmemb)
    {
        FATAL("%s", "checked_fwrite detected short write");
    }
}

/*! \brief Checked wrapper for fread
 *
 * Only for use in cases where "exit immediately" is an acceptable response.
 */
void checked_fread(void *buf, size_t size, size_t nmemb, FILE *stream)
{
    size_t s = fread(buf, size, nmemb, stream);
    if (s != nmemb)
    {
        FATAL("%s", "checked_fread detected short read");
    }
}

void save_game(void)
{
    FILE *fp;
    int i;
    fp = checked_fopen("phasecorr.sav", "wb");
    if (fp == NULL)
    {
        FATAL("couldn't open save file for write: %s", strerror(errno));
    }
    ser_map(fp);
    for (i = 0; i < MAX_PERMON_SLOTS; ++i)
    {
        ser_Permon_stat(fp, permon_stats + i);
    }
    for (i = 0; i < MAX_PERMOBJ_SLOTS; ++i)
    {
        ser_Permobj_stat(fp, permobj_stats + i);
    }
    for (i = 0; i < MAX_MONS_IN_PLAY; ++i)
    {
        ser_Mon(fp, ptr_from_mon(i));
    }
    ser_int32(fp, obj_pool_count);
    for (i = 0; i < obj_pool_count; ++i)
    {
        int j;
        ser_int32(fp, obj_pools[i].capacity);
        for (j = 0; j < obj_pools[i].capacity; ++j)
        {
            ser_Obj(fp, obj_pools[i].data + j);
        }
    }
    ser_Player(fp, &u);
    /* Clean up */
    fflush(fp);
    fclose(fp);
    /* Compress! */
    i = system("gzip phasecorr.sav");
    if (i == -1)
    {
        FATAL("%s", "system() returned -1");
    }
    else if (WIFEXITED(i))
    {
        if (WEXITSTATUS(i) != 0)
        {
            FATAL("gzip returned non-zero exit status %d", WEXITSTATUS(i));
        }
    }
    else if (WIFSIGNALED(i))
    {
        FATAL("gunzip of save file received fatal signal %d", WTERMSIG(i));
    }
    game_finished = 1;
    return;
}

void load_game(void)
{
    FILE *fp;
    int i;
    int32_t local_obj_pools;
    i = system("gunzip phasecorr.sav");
    if (i == -1)
    {
        FATAL("%s", "system() returned -1");
    }
    else if (WIFEXITED(i))
    {
        if (WEXITSTATUS(i) != 0)
        {
            FATAL("gunzip returned non-zero exit status %d", WEXITSTATUS(i));
        }
    }
    else if (WIFSIGNALED(i))
    {
        FATAL("gunzip of save file received fatal signal %d", WTERMSIG(i));
    }
    fp = fopen("phasecorr.sav", "rb");
    room_reset(0);
    deser_map(fp);
    for (i = 0; i < MAX_PERMON_SLOTS; ++i)
    {
        deser_Permon_stat(fp, permon_stats + i);
    }
    for (i = 0; i < MAX_PERMOBJ_SLOTS; ++i)
    {
        deser_Permobj_stat(fp, permobj_stats + i);
    }
    for (i = 0; i < MAX_MONS_IN_PLAY; ++i)
    {
        deser_Mon(fp, varptr_from_mon(i));
    }
    deser_int32(fp, &local_obj_pools);
    for (i = 0; i < local_obj_pools; ++i)
    {
        int j;
        int32_t cap;
        Obj_pool *op;
        deser_int32(fp, &cap);
        op = vivify_obj_pool(cap);
        for (j = 0; j < cap; ++j)
        {
            deser_Obj(fp, op->data + j);
        }
    }
    objects = obj_pools[0].data;
    deser_Player(fp, &u);
    fclose(fp);
    rebuild_roomnums();
    rebuild_mapobjs();
    rebuild_mapmons();
    unlink("phasecorr.sav");
    status_updated = true;
    map_updated = true;
    game_started = true;
    recalc_derived();
    print_msg("Game loaded.\n");
}

/* serdes.c */
