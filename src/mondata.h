/*! \file mondata.h
 * \brief mon struct header file for Phase Correspondence
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

/*! \brief data about an instantiated monster */
typedef struct mon {
    int32_t mon_id; //!< permon index
    int32_t y; //!< y-coord
    int32_t x; //!< x-coord
    int8_t map_num; //!< which map is it on?
    int8_t layer; //!< which map layer is it on?
    bool used; //!< validity flag
    int16_t hpmax; //!< max hit points
    int16_t hpcur; //!< current hit points
    int16_t tohit; //!< to-hit bonus
    int16_t evasion; //!< evasion bonus
    int16_t dam; //!< number of sides on damage die
    Faction current_faction; //!< whose side is it on?
    bool awake; //!< is it allowed to act?
    int32_t ai_lasty; //!< last known y-coord of player
    int32_t ai_lastx; //!< last known x-coord of player
    int8_t ai_lastlayer; //!< last known layer of player
} Mon;

/*! \brief Value indicating "not a monster" */
#define NO_MON (-1)

GLOBALDATA Mon monsters[MAX_MONS_IN_PLAY];

typedef struct mon_pool
{
    int32_t capacity; //!< total number of entries
    Mon *data; //!< pointer to the memory associated with the pool
} Mon_pool;

GLOBALDATA Mon_pool *mon_pools;

#define POOLED_MON(p,n) (mon_pools[(p)].data[(n)])

/* mondata.h */
