/*! \file dice.c
 * \brief Dice-rolling API
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "central.h"
#include "prng.h"
#ifdef GETTIMEOFDAY_HAS_REAL_USEC
#include <sys/time.h>
#endif
#include <time.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

ChaCha20PRNG running_generator;
ChaCha20PRNG saved_generator;

void save_rng_state(void)
{
    memcpy(&saved_generator, &running_generator, sizeof running_generator);
}

void dice_init(bool use_strong)
{
    uint32_t keybuf[CHACHA_KEY_WORDS];
    uint32_t noncebuf[CHACHA_NONCE_WORDS];
#ifdef USE_DEV_URANDOM
    /* On platforms where we have access to a PRNG device node, obtain a key
     * and nonce by reading data from it. */
    if (use_strong)
    {
        /* Initialize the key and nonce by reading data from the system
         * random number device. */
        int fd;
        fd = open("/dev/urandom", O_RDONLY, 0);
        if (fd != -1)
        {
            ssize_t s;
            s = read(fd, keybuf, sizeof keybuf);
            if (s != sizeof keybuf)
            {
                FATAL("%s", "rnum_init: short or failed read from /dev/urandom");
            }
            s = read(fd, noncebuf, sizeof noncebuf);
            if (s != sizeof noncebuf)
            {
                FATAL("%s", "rnum_init: short or failed read from /dev/urandom");
            }
        }
        close(fd);
    }
    else
#endif
    {
        /* Fallback: Initialize the libc rand() implementation from the current
         * system time and use it to populate the key and nonce buffers.  This
         * would be completely inappropriate in a network-facing application,
         * but is tolerable for a simple computer game if you don't mind people
         * being able to predict the starting states of the game. */
        int i;
        int seed;
        NOTICE("%s\n", "Using weak fallback initialization for PRNG");
#ifdef GETTIMEOFDAY_HAS_REAL_USEC
        {
            /* On platforms where the microseconds part of the value emitted
             * by gettimeofday actually means something, cause some mild
             * annoyance to people trying to predict starting states by
             * perturbing the seed with the microsecond part of the current
             * time. */
            struct timeval tv;
            gettimeofday(&tv, NULL);
            seed = tv.tv_sec ^ tv.tv_usec;
        }
#else
        /* The traditional bad PRNG seed: the current system time, to the
         * nearest second. */
        seed = time(NULL);
#endif
        srand(seed);
        for (i = 0; i < 8; ++i)
        {
            keybuf[i] = rand();
        }
        for (i = 0; i < 3; ++i)
        {
            noncebuf[i] = rand();
        }
    }
    init_cc20_prng(&running_generator, keybuf, noncebuf);
}

int exclusive_flat(int lower, int upper)
{
    return lower + one_die(upper - lower - 1);
}

int inclusive_flat(int lower, int upper)
{
    return lower + zero_die(upper - lower + 1);
}

int one_die(int sides)
{
    return 1 + ((sides > 1) ? invoke_cc20_prng(&running_generator) / ((CHACHA_RAND_MAX / (uint32_t) sides) + 1) : 0);
}

int zero_die(int sides)
{
    return ((sides > 1) ? invoke_cc20_prng(&running_generator) / ((CHACHA_RAND_MAX / (uint32_t) sides) + 1) : 0);
}

int dice(int count, int sides)
{
    int total = 0;
    for ( ; count > 0; count--)
    {
        total += one_die(sides);
    }
    return total;
}
/* dice.c */
