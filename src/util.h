/*! \file util.h
 * \brief Header for pure C helper functions
 */

/* Copyright © 2014-2016 Martin Read
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once
#ifndef UTIL_H
#define UTIL_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

int grommile_setup_dirs(char const *vendor, char const *app, int *data_fd, int *config_fd);
int grommile_verify_dir_available(char const *path);
int grommile_makepath(char const *path, int mode);
char *grommile_strbuild(char const *left, char const *right);
int grommile_stdio_mode_to_unistd_flags(char const *mode);
uint8_t grommile_string_bytehash(char const *s);
uint16_t grommile_string_shorthash(char const *s);

struct grommile_kvpair
{
    char *key;
    char *value;
};

struct grommile_kvbucket
{
    int32_t size;
    int32_t reservation;
    struct grommile_kvpair *data;
};

struct grommile_kvstore
{
    struct grommile_kvbucket buckets[256];
};

extern int grommile_store_kv(struct grommile_kvstore *store, char const *key, char const *value);
extern void grommile_delete_kv(struct grommile_kvstore *store, char const *key);
extern char const * grommile_fetch_kv(struct grommile_kvstore *store, char const *key);
extern struct grommile_kvstore *grommile_create_kvstore(int32_t bucket_size_hint);
extern int grommile_init_kvstore(struct grommile_kvstore *store, int32_t bucket_size_hint);

extern char const *grommile_HYGIENIC_STRING;
#ifdef __cplusplus
};
#endif

/*! Users who wish to type less can define the macro GROMMILE_POLLUTE_NAMESPACE
 * to enable macros without the "grommile_" prefix */
#ifdef GROMMILE_POLLUTE_NAMESPACE
#define setup_dirs(...) grommile_setup_dirs(__VA_ARGS__)
#define verify_dir_available(...) grommile_verify_dir_available(__VA_ARGS__)
#define strbuild(...) grommile_strbuild(__VA_ARGS__)
#define stdio_mode_to_unistd_flags(...) grommile_stdio_mode_to_unistd_flags(__VA_ARGS__)
#define string_bytehash(...) grommile_string_bytehash(__VA_ARGS__)
#define string_shorthash(...) grommile_string_shorthash(__VA_ARGS__)
#define HYGIENIC_STRING grommile_HYGIENIC_STRING
#define kvstore grommile_kvstore
#define create_kvstore(...) grommile_create_kvstore(__VA_ARGS__)
#define init_kvstore(...) grommile_init_kvstore(__VA_ARGS__)
#define fetch_kv(...) grommile_fetch_kv(__VA_ARGS__)
#define delete_kv(...) grommile_delete_kv(__VA_ARGS__)
#define store_kv(...) grommile_store_kv(__VA_ARGS__)
#endif

#endif

/*
 * vim:ts=8:sts=4:sw=4:expandtab:cindent
 */
/* util.h */
