/*! \file objects.c
 * \brief Object handling
 */

/* Copyright © 2005, 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "central.h"
#include "objfun.h"
#include "pobjfun.h"
#include "monfun.h"
#include "pmonfun.h"
#include "mapfun.h"
#include "ufun.h"
#include <stdlib.h>
#include <string.h>

/*! \brief pointer to array of object pools */
Obj_pool *obj_pools;
/*! \brief count of allocated pools */
int32_t obj_pool_count;

Obj *objects;
int get_random_pobj(void);

static I32list pobjlut[256];
static I32list pocltables[POCLASS_SHINIES + 1];

static void initialize_obj_pool(Obj_pool *optr, int32_t cap)
{
    int32_t i;
    optr->first_vacancy = 0;
    optr->capacity = cap;
    optr->data = calloc(cap, sizeof (Obj));
    if (optr->data == NULL)
    {
        FATAL("%s", "out of memory initializing new Obj_pool");
    }
    for (i = 0; i < cap; ++i)
    {
        optr->data[i].used = false;
        optr->data[i].obj_id = NO_POBJ;
    }
}

Obj_pool *vivify_obj_pool(int32_t cap)
{
    Obj_pool *tmp;
    obj_pool_count++;
    tmp = realloc(obj_pools, obj_pool_count * sizeof (Obj_pool));
    if (tmp == NULL)
    {
        FATAL("%s", "out of memory creating new entry in obj_pools");
    }
    obj_pools = tmp;
    obj_pools[obj_pool_count - 1].self = obj_pool_count - 1;
    initialize_obj_pool(obj_pools + (obj_pool_count - 1), cap);
    return obj_pools + (obj_pool_count - 1);
}

static void pobjlut_register(int po)
{
    uint8_t hash = grommile_string_bytehash(permobjs[po].name_en);
    i32list_append_uc(pobjlut + hash, po);
}

static void pocltab_register(int po)
{
    enum poclass_num pocl = permobjs[po].poclass;
    i32list_append_uc(pocltables + pocl, po);
}

/*! \brief Consume an object
 *
 * Consumes one object from the object entry referred to by obj.
 *
 * \return true if the entry is now vacant or obj was a bad index
 * \todo implement some kind of alert
 * \param obj index of object to consume
 */
bool consume_obj(int obj)
{
    Obj *optr;
    int i;
    if (obj > MAX_OBJS_IN_PLAY)
    {
        BUG("invalid object index %d > %d passed to consume_obj()!\n", obj, MAX_OBJS_IN_PLAY);
        return true;
    }
    if (obj < 0)
    {
        BUG("invalid object index %d < 0 passed to consume_obj()!\n", obj);
        return true;
    }
    optr = varptr_from_obj(obj);
    if (!optr->used)
    {
        BUG("%s\n", "consume_obj() called on already-released object.");
        return true;
    }
    if (optr->quan < 1)
    {
        BUG("consume_obj() called on object with current quantity %d < 1\n", optr->quan);
        optr->used = false;
        return true;
    }
    --(optr->quan);
    if (optr->quan < 1)
    {
        if (optr->with_you)
        {
            for (i = 0; i <INVENTORY_SIZE; ++i)
            {
                if (u.inventory[i] == obj)
                {
                    u.inventory[i] = NO_OBJ;
                }
            }
        }
        optr->used = false;
        return true;
    }
    return false;
}

int create_obj_class(enum poclass_num po_class, int quantity, int with_you, int depth, int y, int x)
{
    int po_idx;
    int tryct;
    if (po_class == POCLASS_NONE)
    {
        po_idx = -1;
    }
    else if (pocltables[po_class].len == 0)
    {
        return NO_OBJ;
    }
    else
    {
        for (tryct = 0; tryct < 200; tryct++)
        {
            po_idx = pocltables[po_class].list[zero_die(pocltables[po_class].len)];
            if (percentile() < permobjs[po_idx].rarity)
            {
                continue;
            }
            break;
        }
    }
    return create_obj(po_idx, quantity, with_you, depth, y, x);
}

int get_random_pobj(void)
{
    int tryct;
    int po_idx;
    for (tryct = 0; tryct < 200; tryct++)
    {
        po_idx = zero_die(last_populated_permobj + 1);
        if (percentile() < permobjs[po_idx].rarity)
        {
            po_idx = NO_POBJ;
            continue;
        }
        break;
    }
    return po_idx;
}

int create_obj(int po_idx, int quantity, int with_you, int depth, int y, int x)
{
    int i;
    Obj *optr;
    if (objpool_is_full(&obj_pools[0]))
    {
        print_msg("ERROR: Ran out of space in object pool.\n");
        return NO_OBJ;
    }
    if (po_idx == NO_POBJ)
    {
        po_idx = get_random_pobj();
        if (po_idx == NO_POBJ)
        {
            return NO_OBJ;
        }
    }
    i = obj_pools[0].first_vacancy;
    optr = objects + obj_pools[0].first_vacancy;
    optr->obj_id = po_idx;
    if (with_you)
    {
        int slot = get_free_inventory_slot();
        if (slot == NO_SLOT)
        {
            /* inventory full; create object at player's feet instead */
            with_you = false;
            y = u.y;
            x = u.x;
        }
        else
        {
            u.inventory[slot] = i;
            optr->with_you = true;
            y = NO_POS;
            x = NO_POS;
        }
    }
    optr->used = true;
    optr->y = y;
    optr->x = x;
    advance_objpool_vacancy(obj_pools);
    if (permobjs[po_idx].flags[0] & POF0_currency)
    {
        optr->quan = dice(depth + 1, 20);
    }
    else
    {
        optr->quan = quantity;
    }
    if (!optr->with_you)
    {
        SET_OBJ_AT(y,x,i);
    }
    return i;
}

Passfail drop_obj(int inv_idx)
{
    Obj *optr;
    bool implicit_completed;
    optr = varptr_from_obj(u.inventory[inv_idx]);
    if (OBJ_AT(u.y,u.x) == NO_OBJ)
    {
        optr->y = u.y;
        optr->x = u.x;
        SET_OBJ_AT(u.y,u.x,u.inventory[inv_idx]);
        if (u.melee_weap == u.inventory[inv_idx])
        {
            implicit_completed = unwield_melee();
            if (!implicit_completed)
            {
                notify_cant_unwield();
                return PF_fail;
            }
        }
        else if (u.ranged_weap == u.inventory[inv_idx])
        {
            implicit_completed = unwield_ranged();
            if (!implicit_completed)
            {
                notify_cant_unwield();
                return PF_fail;
            }
        }
        u.inventory[inv_idx] = NO_OBJ;
        optr->with_you = 0;
        notify_drop(ptr_from_obj(OBJ_AT(u.y,u.x)));
        return PF_pass;
    }
    else
    {
        notify_drop_blocked();
    }
    return PF_fail;
}

void attempt_pickup(void)
{
    int i;
    int stackable;
    int obj = OBJ_AT(u.y,u.x);
    int po;

    if (obj != NO_OBJ)
    {
        po = permobj_of_obj(obj);
        if (permobjs[po].flags[0] & POF0_currency)
        {
            int32_t d = objects[obj].quan * permobjs[po].dollarval;
            u.moniez += d;
            objects[OBJ_AT(u.y,u.x)].used = false;
            SET_OBJ_AT(u.y,u.x,NO_OBJ);
            map_updated = true;
            notify_currency_gain(d);
            return;
        }
        stackable = po_is_stackable(po);
        if (stackable)
        {
            for (i = 0; i < INVENTORY_SIZE; i++)
            {
                if (permobj_of_obj(u.inventory[i]) == permobj_of_obj(obj))
                {
                    notify_get(obj);
                    objects[u.inventory[i]].quan += objects[obj].quan;
                    objects[obj].used = 0;
                    SET_OBJ_AT(u.y,u.x,NO_OBJ);
                    notify_now_have(i);
                    return;
                }
            }
        }
        i = get_free_inventory_slot();
        if (i == NO_SLOT)
        {
            notify_pack_full();
            return;
        }
        u.inventory[i] = obj;
        SET_OBJ_AT(u.y,u.x,NO_OBJ);
        objects[obj].with_you = true;
        objects[obj].x = NO_POS;
        objects[obj].y = NO_POS;
        notify_now_have(i);
    }
}

/*! \brief Mark a permobj as identified
 *
 * \todo decide whether to permanently inline this or flesh it out
 */
void identify_pobj(int po)
{
    permobj_stats[po].identified = true;
}

/*! \brief Standard inventory filter, selecting a single permobj class */
bool classic_inv_filter(int obj, intptr_t extra)
{
    int pocl = (int) extra;
    return (obj != NO_OBJ) && ((pocl == POCLASS_NONE) || (pocl_of_obj(obj) == pocl));
}

int get_pobj_by_name(char const *name_en)
{
    uint8_t hash;
    assert(name_en != NULL);
    hash = grommile_string_bytehash(name_en);
    int i;
    if (pobjlut[hash].len == 0)
    {
        NOTICE("Item lookup failed for %s\n", name_en);
        return NO_POBJ;
    }
    for (i = 0; i < pobjlut[hash].len; ++i)
    {
        if (!strcmp(name_en, permobjs[pobjlut[hash].list[i]].name_en))
        {
            return pobjlut[hash].list[i];
        }
    }
    return NO_POBJ;
}

void release_obj(Obj *from)
{
    Obj_pool *pool = obj_pools + from->pool;
    from->used = false;
    pool->first_vacancy = min_int32(pool->first_vacancy, from - pool->data);
}

void merge_objs(Obj *into, Obj *from)
{
    if (into->obj_id != from->obj_id)
    {
        FATAL("Merging mismatched objects: into type %d, from type %d\n",
              into->obj_id, from->obj_id);
    }
    into->quan += from->quan;
    release_obj(from);
}

/*! \brief Transfer an object to another pool
 *
 * Transfer the object `from` to the object pool `receiver`.
 *
 * \param from Object to transfer
 * \param receiver Destination object pool.
 */
void transfer_obj(Obj_pool *receiver, Obj *from)
{
    if (from->pool == receiver->self)
    {
        // this probably merits notification at some point.
        return;
    }
    if (objpool_is_full(receiver))
    {
        return;
    }
}

/*! \brief Advance an object pool's first vacancy
 *
 * This function assumes that the currently specified first vacancy of the
 * pool has just been filled, and scans forward to find the next vacancy.
 */
void advance_objpool_vacancy(Obj_pool *op_ptr)
{
    for (; (op_ptr->first_vacancy < op_ptr->capacity) && (op_ptr->data[op_ptr->first_vacancy].used); ++(op_ptr->first_vacancy))
    {
        // EMPTY LOOP
    }
}

/*! \brief Find an object pool's first vacancy directly
 *
 * This function finds the first vacancy in the specified object pool
 * automatically, and updates the relevant field.
 */
void reset_objpool_vacancy(Obj_pool *op_ptr)
{
    int32_t i;
    for (i = 0; i < op_ptr->capacity; ++i)
    {
        if (!op_ptr->data[i].used)
        {
            op_ptr->first_vacancy = i;
            return;
        }
    }
    op_ptr->first_vacancy = op_ptr->capacity;
}

/*! \brief Initialize the object subsystem */
void obj_init(void)
{
    int i;
    for (i = 0; i < MAX_PERMOBJ_SLOTS; ++i)
    {
        if (!permobjs[i].valid)
        {
            last_populated_permobj = i - 1;
            break;
        }
        else
        {
            pobjlut_register(i);
            pocltab_register(i);
        }
    }
    objects = vivify_obj_pool(MAX_OBJS_IN_PLAY)->data;
    return;
}

/* objects.c */
