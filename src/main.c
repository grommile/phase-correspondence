/*! \file main.c
 * \brief main() and friends
 */

/* Copyright © 2005, 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "central.h"
#include "gameio.h"
#include "objfun.h"
#include "pobjfun.h"
#include "monfun.h"
#include "pmonfun.h"
#include "mapfun.h"
#include "ufun.h"
#include "prng.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef GETTIMEOFDAY_HAS_REAL_USEC
#include <sys/time.h>
#endif
#include <time.h>
#include <locale.h>

static void new_game(void);
static void main_loop(void);
static bool do_command(Full_cmd *fcmd);
bool game_finished = false;
bool game_started = false;
int32_t game_tick = 0;
static noreturn void default_fatal(char const *file, int line, char const *fmt, ...);
void NORETURN_FUNCPTR (*fatal)(char const *file, int line, char const *fmt, ...) = default_fatal;

void new_game(void)
{
    u_init();
    make_new_level(u.map_num);
    game_started = true;
    status_updated = true;
    map_updated = true;
    display_update();
}

bool do_command(Full_cmd *fcmd)
{
    int i;
    Passfail pf;
    bool implicit_completed = false;
    switch (fcmd->cmd)
    {
    case MOVE_NORTH:
        return move_player(-1, 0);
    case MOVE_SOUTH:
        return move_player(1, 0);
    case MOVE_EAST:
        return move_player(0, 1);
    case MOVE_WEST:
        return move_player(0, -1);
    case MOVE_NW:
        return move_player(-1, -1);
    case MOVE_NE:
        return move_player(-1, 1);
    case MOVE_SE:
        return move_player(1, 1);
    case MOVE_SW:
        return move_player(1, -1);

    case ATTACK:
        {
            Direction *s = (Direction *) fcmd->extra;
            bool r = player_attack(s->y, s->x);
            free(s);
            return r;
        }

    case GET_ITEM:
        if (OBJ_AT(u.y, u.x) != NO_OBJ)
        {
            attempt_pickup();
            return true;
        }
        else
        {
            print_msg("Nothing to get.\n");
            return false;
        }

    case WIELD_WEAPON:
        {
            int obj = fcmd->extra;
            if (obj == NO_OBJ)
            {
                /* TODO decide whether to announce the UI bug */
                return false;
            }
            if ((u.melee_weap != obj) && (u.ranged_weap != obj))
            {
                if (obj_is_melee_weapon(obj))
                {
                    implicit_completed = (u.melee_weap == NO_OBJ) || unwield_melee();
                    return implicit_completed && wield_weapon(obj);
                }
                else if (obj_is_ranged_weapon(obj))
                {
                    implicit_completed = (u.ranged_weap == NO_OBJ) || unwield_ranged();
                    return implicit_completed && wield_weapon(obj);
                }
            }
            else
            {
                /* If you try to wield the weapon you're already wielding,
                 * nothing at all happens. */
                return false;
            }
        }
        return false;

    case WEAR_ARMOUR:
        {
            int obj = fcmd->extra;
            if (u.armour != NO_OBJ)
            {
                print_msg("You are already wearing armour. Take it off first.\n");
                return false;
            }
            return wear_armour(obj);
        }
        return false;

    case TAKE_OFF_ARMOUR:
        return take_off_armour();

    case GO_DOWN_STAIRS:
        leave_level(u.map_num);
        ++u.map_num;
        make_new_level(u.map_num);
        return false;

    case STAND_STILL:
        return true;

    case USE_CONSUMABLE:
        i = fcmd->extra;
        return use_consumable(u.inventory[i]);

    case REMOVE_PLUGIN:
        if (u.plugin == NO_OBJ)
        {
            print_msg("You have no plugin module to remove!\n");
            return false;
        }
        return remove_plugin();

    case PLUG_IN_PLUGIN:
        return plug_in_plugin(fcmd->extra);

    case DROP_ITEM:
        i = fcmd->extra;
        if (i >= 0)
        {
            if (inv_slot_occupied(i) &&
                ((u.inventory[i] == u.plugin) ||
                 (u.inventory[i] == u.armour)))
            {
                print_msg("You cannot drop something you are wearing.\n");
                return false;
            }
            pf = drop_obj(i);
            return (pf == PF_pass);
        }
        return false;

    case SAVE_GAME:
        game_finished = true;
        save_game();
        return false;

    case UNWIELD_RANGED:
        return unwield_ranged();

    case UNWIELD_MELEE:
        return unwield_melee();

    case QUIT:
        game_finished = true;
        return false;
    }
    return false;
}

void main_loop(void)
{
    Full_cmd fcmd;
    int i;
    Speed action_speed;
    while (!game_finished)
    {
        switch (game_tick & 3)
        {
        case 0:
        case 2:
            action_speed = Speed_slow;
            break;
        case 1:
            action_speed = Speed_normal;
            break;
        case 3:
            action_speed = Speed_fast;
            break;
        }
        /* Player is always speed 1, for now. */
        if (action_speed <= 1)
        {
            bool didcmd = false;
            do
            {
                /* Take commands until the player does
                 * something that uses an action. */
                bool gotcmd = get_command(&fcmd);
                if (gotcmd)
                {
                    didcmd = do_command(&fcmd);
                }
                if (game_finished)
                {
                    break;
                }
            } while (!didcmd);
            if (game_finished)
            {
                break;
            }
        }
        for (i = 0; i < MAX_MONS_IN_PLAY; i++)
        {
            if (monsters[i].used == 0)
            {
                /* Unused monster */
                continue;
            }
            if (action_speed <= permons[monsters[i].mon_id].speed)
            {
                mon_acts(i);
            }
            if (game_finished)
            {
                break;
            }
        }
        if (game_finished)
        {
            break;
        }
        bookkeep_u();
        game_tick++;
    }
}

static noreturn void default_fatal(char const *file, int line, char const *fmt, ...)
{
    va_list ap;
    display_shutdown(true);
    fprintf(stderr, "FATAL ERROR (%s:%d)\n", file, line);
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fputc('\n', stderr);
    abort();
}

void notice(char const *file, int line, char const *fmt, ...)
{
    print_msg("notice (%s:%d): ", file, line);
    va_list ap;
    va_start(ap, fmt);
    vprint_msg(fmt, ap);
    va_end(ap);
}

void bug(char const *file, int line, char const *fmt, ...)
{
    print_msg("\nBUG (%s:%d): ", file, line);
    va_list ap;
    va_start(ap, fmt);
    vprint_msg(fmt, ap);
    va_end(ap);
}

int main(void)
{
    struct stat s;
    int i;
    setlocale(LC_ALL, "");
    load_config();
    display_init();
    dice_init(true);
    mon_init();
    obj_init();
    /* Do we have a saved game? */
    i = stat("phasecorr.sav.gz", &s);
    if (!i)
    {
        /* Yes! */
        print_msg("Loading saved game...\n");
        load_game();
    }
    else
    {
        /* No! */
        print_msg("Welcome to Phase Correspondence!\n");
        print_msg("Copyright © 2016 Martin Read.\n");
        new_game();
    }
    display_update();
    main_loop();
    display_shutdown(false);
    return 0;
}

/* main.c */
