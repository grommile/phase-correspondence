/*! \file geometry.c
 * \brief Manipulations of coordinates under a Chebybshev metric
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#define GEOMETRY_C
#include "central.h"

#define BASIC_ACTION(i, y, x) do { new_dy[(i)] = (y); new_dx[(i)] = (x); } while (0)
#define FOLLOW_THE_ARROWS(i) BASIC_ACTION((i), mysign((dy)), mysign((dx)))
#define AGAINST_THE_ARROWS(i) BASIC_ACTION((i), -mysign((dy)), -mysign((dx)))
#define DX_ONLY(i, dx) BASIC_ACTION((i), 0, mysign((dx)))
#define DY_ONLY(i, dy) BASIC_ACTION((i), 0, mysign((dy)))
#define FLOW_X_PLUS(i, dx) BASIC_ACTION((i), 1, mysign((dx)))
#define FLOW_X_MINUS(i, dx) BASIC_ACTION((i), -1, mysign((dx)))
#define FLOW_Y_PLUS(i, dy) BASIC_ACTION((i), 1, mysign((dy)))
#define FLOW_Y_MINUS(i, dy) BASIC_ACTION((i), -1, mysign((dy)))

/*! \brief converge on a diagonal, preferably approaching the goal
 *
 * \param dy y-part of vector towards goal
 * \param dx x-part of vector towards goal
 * \param new_dy buffer for y-parts of proposed steps
 * \param new_dx buffer for x-parts of proposed steps
 * \param max_points Maximum number of points to record into the buffers
 * \return Number of points actually recorded into the buffers
 */
int geom_towards_diagonal(int dy, int dx, int *restrict new_dy, int *restrict new_dx, int max_points)
{
    if (max_points < 1)
    {
        return 0;
    }
    int true_points = 0;
    int dia_dist = geom_dist_from_diagonal(dy, dx);
    if (dia_dist == 0)
    {
        FOLLOW_THE_ARROWS(0);
        return 1;
    }
    return true_points;
}

/*! \brief Move towards a goal, preferably converging on an orthogonal
 *
 * \param dy y-part of vector towards goal
 * \param dx x-part of vector towards goal
 * \param new_dy buffer for y-parts of proposed steps
 * \param new_dx buffer for x-parts of proposed steps
 * \param max_points Maximum number of points to record into the buffers
 * \return Number of points actually recorded into the buffers
 */
int geom_towards_orthogonal(int dy, int dx, int *restrict new_dy, int *restrict new_dx, int max_points)
{
    if (max_points < 1)
    {
        return 0;
    }
    int ortho_dist = geom_dist_from_orthogonal(dy, dx);
    if (ortho_dist == 0)
    {
        FOLLOW_THE_ARROWS(0);
        return 1;
    }
    return 0;
}

/*! \brief Move towards a goal
 *
 * The order of the second and third points is deterministic even if the
 * absolute values of dy and dx are equal. Callers desiring stochastic
 * decision making in that case are responsible for performing their own
 * randomized transposition.
 *
 * \param dy y-part of vector towards goal
 * \param dx x-part of vector towards goal
 * \param new_dy buffer for y-parts of proposed steps
 * \param new_dx buffer for x-parts of proposed steps
 * \param max_points Maximum number of points to record into the buffers
 * \return Number of points actually recorded into the buffers
 */
int geom_towards_goal(int dy, int dx, int *restrict new_dy, int *restrict new_dx, int max_points)
{
    if (max_points < 1)
    {
        return 0;
    }
    switch (max_points)
    {
    default:
    case 3:
        if (dy == 0)
        {
            FLOW_X_MINUS(2, dx);
        }
        else if (dx == 0)
        {
            FLOW_Y_MINUS(2, dy);
        }
        else if (myabs(dy) >= myabs(dx))
        {
            DX_ONLY(2, dx);
        }
        else
        {
            DY_ONLY(2, dy);
        }
        // FALLTHRU
    case 2:
        if (dy == 0)
        {
            FLOW_X_PLUS(1, dx);
        }
        else if (dx == 0)
        {
            FLOW_Y_PLUS(1, dy);
        }
        else if (myabs(dy) < myabs(dx))
        {
            DX_ONLY(1, dx);
        }
        else
        {
            DY_ONLY(1, dy);
        }
        // FALLTHRU
    case 1:
        FOLLOW_THE_ARROWS(0);
        return min_int(max_points, 3);
    }
}

/*! \brief Move away from the goal.
 *
 * Seeks to return up to seven points that are not closer to the goal.
 *
 * The author recommends that callers use either max_points == 7 or
 * max_points == 5. Smaller values of max_points may result in a biased set
 * of points.
 *
 * The author also anticipates that this algorithm will be prone to "pathing
 * flap" (because it isn't really a pathing algorithm).  This is an acceptable
 * representation of scared things succumbing to panic.
 *
 * \param dy y-part of vector towards goal
 * \param dx x-part of vector towards goal
 * \param new_dy buffer for y-parts of proposed steps
 * \param new_dx buffer for x-parts of proposed steps
 * \param max_points Maximum number of points to record into the buffers
 * \return Number of points actually recorded into the buffers
 */
int geom_flee_goal(int dy, int dx, int *restrict new_dy, int *restrict new_dx, int max_points)
{
    int sy = mysign(dy);
    int sx = mysign(dx);
    if (sy == 0)
    {
        switch (max_points)
        {
        default:
        case 7:
            // worst cases close the range but try to either open the angle
            // or flow around the goal depending on where we are relative to it
            FLOW_X_MINUS(6, dx);
            // FALLTHRU
        case 6:
            FLOW_X_PLUS(5, dx);
            // FALLTHRU
        case 5:
            // holding range but opening angle is better than closing range
            DY_ONLY(4, -1);
            // FALLTHRU
        case 4:
            DY_ONLY(3, 1);
            // FALLTHRU
        case 3:
            // directly away is third best
            AGAINST_THE_ARROWS(2);
            // FALLTHRU
        case 2:
            // best cases here are either side of directly away
            FLOW_X_MINUS(1, -dx);
            // FALLTHRU
        case 1:
            FLOW_X_PLUS(0, -dx);
            return min_int(max_points, 7);
        case 0:
            return 0;
        }
    }
    else if (sx == 0)
    {
        switch (max_points)
        {
        default:
        case 7:
            // worst cases close the range but try to either open the angle
            // or flow around the goal depending on where we are relative to it
            FLOW_Y_MINUS(6, dx);
            // FALLTHRU
        case 6:
            FLOW_Y_PLUS(5, dx);
            // FALLTHRU
        case 5:
            // holding range but opening angle is better than closing range
            DX_ONLY(4, -1);
            // FALLTHRU
        case 4:
            DX_ONLY(3, 1);
            // FALLTHRU
        case 3:
            // directly away is third best
            AGAINST_THE_ARROWS(2);
            // FALLTHRU
        case 2:
            // best cases here are either side of directly away
            FLOW_Y_MINUS(1, -dx);
            // FALLTHRU
        case 1:
            FLOW_Y_PLUS(0, -dx);
            return min_int(max_points, 7);
        case 0:
            return 0;
        }

    }
    else
    {
        switch (max_points)
        {
        default:
        case 7:
            DX_ONLY(6, dx);
            // FALLTHRU
        case 6:
            DY_ONLY(5, dy);
            // FALLTHRU
        case 5:
            BASIC_ACTION(4, mysign(-dx), mysign(dy));
            // FALLTHRU
        case 4:
            BASIC_ACTION(3, mysign(dx), mysign(-dy));
            // FALLTHRU
        case 3:
            if (myabs(dy) >= myabs(dx))
            {
                DX_ONLY(2, -dx);
            }
            else
            {
                DY_ONLY(2, -dy);
            }
            // FALLTHRU
        case 2:
            if (myabs(dy) < myabs(dx))
            {
                DX_ONLY(1, -dx);
            }
            else
            {
                DY_ONLY(1, -dy);
            }
            // FALLTHRU
        case 1:
            // If we're off the orthogonals, directly away is best
            AGAINST_THE_ARROWS(0);
            return min_int(max_points, 7);
        case 0:
            return 0;
        }
    }
    return 0;
}

/* geometry.c */
