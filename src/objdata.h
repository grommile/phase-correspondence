/*! \file objdata.h
 * \brief obj struct header file for Phase Correspondence
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

/*! \brief data about an instantiated object */
typedef struct obj {
    int32_t obj_id; /*!< permobj index */
    int32_t pool; /*!< What pool is the object in? */
    int32_t quan; /*!< How many? Most items don't stack. */
    bool with_you; /*!< in player inventory */
    int32_t y; /*!< y coordinate if not carried */
    int32_t x; /*!< x coordinate if not carried */
    bool used; /*!< Entry is occupied. */
} Obj;

/*! \brief Value indicating "not an object" */
#define NO_OBJ (-1)

extern Obj *objects;

typedef struct obj_pool
{
    int32_t self;
    int32_t capacity;
    int32_t first_vacancy;
    Obj *data;
} Obj_pool;

GLOBALDATA Obj_pool *obj_pools;
GLOBALDATA int32_t obj_pool_count;

#define POOLED_OBJ(p,n) (obj_pools[(p)].data[(n)])

/* objdata.h */
