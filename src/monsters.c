/*! \file monsters.c
 * \brief monster-related functionality
 */

/* Copyright © 2005, 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "central.h"
#include "objfun.h"
#include "pobjfun.h"
#include "monfun.h"
#include "mapfun.h"
#include "pmonfun.h"
#include "ufun.h"
#include <stdlib.h>
#include <string.h>

Mon_pool *mon_pools;

static I32list pmonlut[256];

static void pmonlut_register(int pm)
{
    uint8_t hash = grommile_string_bytehash(permons[pm].name_en);
    ++pmonlut[hash].len;
    pmonlut[hash].list = realloc(pmonlut[hash].list, pmonlut[hash].len * sizeof (int32_t));
    pmonlut[hash].list[pmonlut[hash].len - 1] = pm;
}

Mon monsters[MAX_MONS_IN_PLAY];
static int reject_mon(int pm, int depth);

int get_random_pmon(int depth)
{
    int tryct;
    int pm;
    for (tryct = 0; tryct < 200; tryct++)
    {
        pm = zero_die(last_populated_permon + 1);
        if (reject_mon(pm, depth))
        {
            pm = NO_PMON;
            continue;
        }
        break;
    }
    return pm;
}

int create_mon(int pm_idx, int depth, int y, int x)
{
    int mon;
    if (pm_idx == NO_PMON)
    {
        pm_idx = get_random_pmon(depth);
        if (pm_idx == NO_PMON)
        {
            return NO_MON;
        }
    }
    for (mon = 0; mon < MAX_MONS_IN_PLAY; mon++)
    {
        if (!monsters[mon].used)
        {
            monsters[mon].mon_id = pm_idx;
            monsters[mon].used = true;
            monsters[mon].y = y;
            monsters[mon].x = x;
            monsters[mon].hpmax = permons[pm_idx].hp;
            monsters[mon].hpcur = monsters[mon].hpmax;
            monsters[mon].tohit = permons[pm_idx].tohit;
            monsters[mon].evasion = permons[pm_idx].evasion;
            monsters[mon].dam = permons[pm_idx].dam;
            monsters[mon].awake = false;
            SET_MON_AT(y,x,mon);
            permon_stats[pm_idx].num_generated++;
            return mon;
        }
    }
    return NO_MON;
}

void death_drop(int mon)
{
    int pm = monsters[mon].mon_id;
    int y = monsters[mon].y;
    int x = monsters[mon].x;
    int dy, dx;
    int tryct;
    for (tryct = 0; (tryct < 100) && ((OBJ_AT(y,x) != NO_OBJ) || (TERR_AT(y,x) != FLOOR)); ++tryct)
    {
        dy = zero_die(3) - 1;
        dx = zero_die(3) - 1;
        tryct++;
        y += dy;
        x += dx;
    }
    if (tryct < 100)
    {
        switch (pm)
        {
        default:
            break;
        }
    }
}

bool mon_can_pass(int mon, int y, int x)
{
    if (pos_is_nowhere(y, x) || pos_is_oob(y, x))
    {
        return false;
    }
    if (MON_AT(y,x) != NO_MON)
    {
        return false;
    }
    if ((y == u.y) && (x == u.x))
    {
        /* Sanity check! */
        return false;
    }
    if (TERR_AT(y,x) == WALL)
    {
        return (permons[monsters[mon].mon_id].flags[0] & PMF0_wallwalk);
    }
    return true;
}

void damage_mon(int mon, int amount, bool by_you, int killer)
{
    Mon *mptr;
    mptr = varptr_from_mon(mon);
    if (amount >= mptr->hpcur)
    {
        if (by_you)
        {
            permon_stats[mptr->mon_id].killed_by_player++;
            notify_ukillm(mptr);
        }
        else
        {
            permon_stats[mptr->mon_id].killed_by_other++;
            notify_mkillm(mptr, ptr_from_mon(killer));
        }
        death_drop(mon);
        SET_MON_AT(mptr->y,mptr->x,NO_MON);
        mptr->used = false;
        map_updated = true;
        display_update();
    }
    else
    {
        mptr->hpcur -= amount;
    }
}

int reject_mon(int pm, int depth)
{
    if ((permons[pm].power > depth) || (percentile() < permons[pm].rarity))
    {
        return 1;
    }
    return 0;
}

void move_mon(int mon, int y, int x)
{
    Mon *mptr;
    mptr = varptr_from_mon(mon);
    SET_MON_AT(mptr->y,mptr->x,NO_MON);
    mptr->y = y;
    mptr->x = x;
    SET_MON_AT(mptr->y,mptr->x,mon);
    map_updated = 1;
    display_update();
}

void mon_acts(int mon)
{
    Mon *mptr;
    int pm;
    int dy, dx;
    int y, x;
    int sy, sx;
    mptr = varptr_from_mon(mon);
    pm = mptr->mon_id;
    /* dy,dx == direction monster must go to reach you. */
    dy = u.y - mptr->y;
    dx = u.x - mptr->x;
    y = mptr->y;
    x = mptr->x;
    if (cheby_magnitude(dy, dx) < 2)
    {
        mhitu(mon);
    }
    else if ((REGION_AT(u.y,u.x) != NO_ROOM) && (REGION_AT(u.y,u.x) == REGION_AT(mptr->y, mptr->x)))
    {
        /* In same room. */
        if (!mptr->awake)
        {
            mptr->awake = true;
            notify_mon_wakes(ptr_from_mon(mon));
        }
        select_space(&y, &x, u.y, u.x, permons[pm].ai);
        move_mon(mon, y, x);
    }
    else if (REGION_AT(y,x) == NO_ROOM)
    {
        /* In a corridor. */
        sy = dy ? ((dy > 0) ? 1 : -1) : 0;
        sx = dx ? ((dx > 0) ? 1 : -1) : 0;
        if (sy && ((TERR_AT(y + sy,x) == FLOOR) && (MON_AT(y + sy,x) == NO_MON)))
        {
            move_mon(mon, y + sy, x);
        }
        else if (sx && ((TERR_AT(y,x + sx) == FLOOR) && (MON_AT(y,x + sx) == NO_MON)))
        {
            move_mon(mon, y, x + sx);
        }
    }
    else
    {
        /* Different room. For now, do nothing. */
    }
}

int get_pmon_by_name(char const *name_en)
{
    uint8_t hash = grommile_string_bytehash(name_en);
    int i;
    if (pmonlut[hash].len == 0)
    {
        return NO_PMON;
    }
    for (i = 0; i < pmonlut[hash].len; ++i)
    {
        if (!strcmp(name_en, permons[pmonlut[hash].list[i]].name_en))
        {
            return pmonlut[hash].list[i];
        }
    }
    return NO_PMON;
}

void mon_init(void)
{
    int i;
    for (i = 0; i < MAX_PERMON_SLOTS; ++i)
    {
        if (!permons[i].valid)
        {
            last_populated_permon = i - 1;
            break;
        }
        pmonlut_register(i);
    }
    return;
}

/* monsters.c */
