/*! \file playerdata.h
 * \brief The player struct for Phase Correspondence
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/*! \brief Ways for the player to die */
typedef enum death {
    DEATH_KILLED, DEATH_KILLED_MON
} Death;

/*! \brief The player character */
typedef struct player {
    char name[PLAYER_NAME_DISPLAYED + 1]; /*!< player character name */
    int32_t y; /*!< y-coordinate of current position */
    int32_t x; /*!< x-coordinate of current position */
    Damagenum hpmax; /*!< Max hit points; max of 999. */
    Damagenum hpcur; /*!< Current hit points; <= 0 is dead. */
    Damagenum shmax; /*!< Max shield; max of 999. */
    Damagenum shcur; /*!< Current shield; if it's gone, damage affects HP. */
    Accuracynum tohit; /*!< Current accuracy. */
    Accuracynum evasion; /*!< Current evasion. */
    Gametimenum shield_timer; /*!< Next tick on which your shield is allowed to regenerate */
    Damagenum shield_regen; /*!< Shield regeneration rate in points/turn */
    Tinynum level; /*!< gain levels to get more powerful - lockstepped */
    Tinynum map_num; /*!< what map are we on? */
    Tinynum layer; /*!< what map *layer* are we on? */
    int32_t moniez; /*!< futile metric of score  */
    int32_t invpool; /*!< reference to inventory pool */
    Objnum inventory[INVENTORY_SIZE]; /*!< items carried */
    Objnum melee_weap; /*!< currently equipped melee weapon */
    Objnum ranged_weap; /*!< currently equipped ranged weapon */
    Objnum armour; /*!< currently equipped armour */
    Objnum plugin; /*!< currently equipped plug-in module */
} Player;

/*! \brief Refer to object from player's inventory pool */
#define INVENTORY_OBJ(n) POOLED_OBJ(u.invpool, (n))

GLOBALDATA Player u;

/* playerdata.h */
