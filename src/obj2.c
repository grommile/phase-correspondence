/*! \file obj2.c
 * \brief Second object handling module - equipment and consumables
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "central.h"
#include "objfun.h"
#include "pobjfun.h"
#include "ufun.h"

/*! \brief Equip a piece of armour */
bool wear_armour(int obj)
{
    if (obj_is_armour(obj))
    {
        u.armour = obj;
        recalc_derived();
        notify_wear(obj);
        return true;
    }
    else
    {
        BUG("%s\n", "UI permitted attempt to wear non-armour");
        return false;
    }
}

/*! \brief Unequip a piece of armour */
bool take_off_armour(void)
{
    int obj = u.armour;
    if (obj != NO_OBJ)
    {
        u.armour = NO_OBJ;
        recalc_derived();
        notify_take_off(obj);
        return true;
    }
    else
    {
        NOTICE("%s\n", "take_off_armour() called when no armour equipped");
        return false;
    }
}

/*! \brief Equip a weapon */
bool wield_weapon(int obj)
{
    if (obj == NO_OBJ)
    {
        BUG("%s\n", "attempt to wield NO_OBJ");
        return false;
    }
    if (obj_is_melee_weapon(obj))
    {
        u.melee_weap = obj;
        recalc_derived();
        notify_wield(u.melee_weap);
        return true;
    }
    else if (obj_is_ranged_weapon(obj))
    {
        u.ranged_weap = obj;
        recalc_derived();
        notify_ready_ranged(u.ranged_weap);
        return true;
    }
    else
    {
        BUG("%s\n", "UI permitted attempt to wield non-weapon");
        return false;
    }
}

/*! \brief Set aside the player's currently equipped ranged weapon. */
bool unwield_ranged(void)
{
    if (u.ranged_weap == NO_OBJ)
    {
        NOTICE("%s\n", "unwield_ranged() called when no ranged weapon wielded");
        return false;
    }
    u.ranged_weap = NO_OBJ;
    recalc_derived();
    print_msg("Ranged weapon unwielded.\n");
    return true;
}

/*! \brief Set aside the player's currently equipped melee weapon. */
bool unwield_melee(void)
{
    if (u.melee_weap == NO_OBJ)
    {
        NOTICE("%s\n", "unwield_melee() called when no melee weapon wielded");
        return false;
    }
    u.melee_weap = NO_OBJ;
    recalc_derived();
    print_msg("Melee weapon unwielded.\n");
    return true;
}

/*! \brief Equip a plugin module
 *
 * \param obj index of object to equip
 */
bool plug_in_plugin(int obj)
{
    if (obj_is_plugin(obj))
    {
        u.plugin = obj;
        recalc_derived();
        notify_plug_in(obj);
        return true;
    }
    else
    {
        BUG("%s\n", "UI permitted attempt to put on non-ring");
        return false;
    }
}

/*! \brief Unequip current ring */
bool remove_plugin(void)
{
    int obj = u.plugin;
    if (obj != NO_OBJ)
    {
        u.plugin = NO_OBJ;
        recalc_derived();
        notify_remove(obj);
        return true;
    }
    else
    {
        NOTICE("%s\n", "removing ring when no ring equipped");
        return false;
    }
}

static void eff_boost_shield(int obj)
{
    int po = permobj_of_obj(obj);
    int potency = permobjs[po].power.potency;
    boost_shield(potency);
    status_updated = true;
}

static void eff_restore_shield(int obj)
{
    int po = permobj_of_obj(obj);
    int potency = permobjs[po].power.potency;
    int32_t tmp = potency;
    tmp *= u.shmax;
    tmp += 99;
    tmp /= 100;
    restore_shield(tmp);
}

static void eff_restore_health(int obj)
{
    int po = permobj_of_obj(obj);
    int potency = permobjs[po].power.potency;
    int32_t tmp = potency;
    tmp *= u.hpmax;
    tmp += 99;
    tmp /= 100;
    heal_u(tmp, false);
}

static void eff_monster_sense(int obj __attribute__((unused)))
{
    return;
}

typedef void(*Effect_fptr)(int obj);

Effect_fptr effect_funcs[] = {
    eff_restore_shield,
    eff_boost_shield,
    eff_restore_health,
    eff_monster_sense,
};

bool use_consumable(int obj)
{
    if (obj != NO_OBJ)
    {
        int po = permobj_of_obj(obj);
        if (permobjs[po].power.effect != EFFECT_nil)
        {
            effect_funcs[permobjs[po].power.effect](obj);
            consume_obj(obj);
        }
        else
        {
            notify_use_useless();
        }
        return true;
    }
    return false;
}

/* obj2.c */
