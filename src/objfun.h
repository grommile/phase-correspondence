/*! \file objfun.h
 * \brief object functions header file
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#ifndef INC_objfun_h
#define INC_objfun_h

#include "central.h"

GLOBALFUNC void obj_init(void);
GLOBALFUNC void print_obj_name(Obj const *optr);
GLOBALFUNC int create_obj(int po_idx, int quantity, int with_you, int depth, int y, int x);
GLOBALFUNC int drop_obj(int inv_idx);
GLOBALFUNC int create_obj_class(enum poclass_num pocl, int quantity, int with_you, int depth, int y, int x);
GLOBALFUNC int create_obj_random(int y, int x);
GLOBALFUNC bool use_consumable(int obj);
GLOBALFUNC bool wield_weapon(int obj);
GLOBALFUNC bool wear_armour(int obj);
GLOBALFUNC bool plug_in_plugin(int obj);
GLOBALFUNC bool take_off_armour(void);
GLOBALFUNC bool unwield_melee(void);
GLOBALFUNC bool unwield_ranged(void);
GLOBALFUNC bool remove_plugin(void);
GLOBALFUNC void attempt_pickup(void);
GLOBALFUNC bool consume_obj(int obj);
GLOBALFUNC bool classic_inv_filter(int obj, intptr_t extra) PUREFUNC;


/*! \brief get a pointer to const Obj from an index */
INLINEFUNC Obj const * ptr_from_obj(int obj)
{
    return objects + obj;
}

/*! \brief get a pointer to non-const Obj from an index */
INLINEFUNC Obj * varptr_from_obj(int obj)
{
    return objects + obj;
}

/*! \brief find the po of an obj */
INLINEFUNC int permobj_of_obj(int obj)
{
    return objects[obj].obj_id;
}

/*! \brief find the poclass of an obj */
INLINEFUNC int pocl_of_obj(int obj)
{
    return permobjs[permobj_of_obj(obj)].poclass;
}

/*! \brief predicate: is the object armour? */
INLINEFUNC bool obj_is_armour(int obj)
{
    return pocl_of_obj(obj) == POCLASS_ARMOUR;
}

/*! \brief predicate: is the object a weapon? */
INLINEFUNC bool obj_is_weapon(int obj)
{
    return pocl_of_obj(obj) == POCLASS_WEAPON;
}

/*! \brief predicate: is the object a melee weapon? */
INLINEFUNC bool obj_is_melee_weapon(int obj)
{
    return obj_is_weapon(obj) && !(permobjs[objects[obj].obj_id].flags[0] & POF0_ranged);
}

/*! \brief predicate: is the object a ranged weapon? */
INLINEFUNC bool obj_is_ranged_weapon(int obj)
{
    return obj_is_weapon(obj) && (permobjs[objects[obj].obj_id].flags[0] & POF0_ranged);
}

/*! \brief predicate: is the object useless loot? */
INLINEFUNC bool obj_is_shiny(int obj)
{
    return pocl_of_obj(obj) == POCLASS_SHINIES;
}

/*! \brief predicate: is the object a consumable? */
INLINEFUNC bool obj_is_consumable(int obj)
{
    return pocl_of_obj(obj) == POCLASS_CONSUMABLE;
}

/*! \brief predicate: is the object a plugin? */
INLINEFUNC bool obj_is_plugin(int obj)
{
    return pocl_of_obj(obj) == POCLASS_PLUGIN;
}

GLOBALFUNC Obj_pool *vivify_obj_pool(int32_t cap);
GLOBALFUNC void merge_objs(Obj *into, Obj *from);
GLOBALFUNC void release_obj(Obj *from);
GLOBALFUNC void transfer_obj(Obj_pool *receiver, Obj *from);
GLOBALFUNC void reset_objpool_vacancy(Obj_pool *op_ptr);
GLOBALFUNC void advance_objpool_vacancy(Obj_pool *op_ptr);

INLINEFUNC bool objpool_is_full(Obj_pool *op_ptr)
{
    return (op_ptr->first_vacancy >= op_ptr->capacity);
}

#endif

/* objfun.h */
