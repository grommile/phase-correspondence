/*! \file u.c
 * \brief Player handling
 */

/* Copyright © 2005, 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "central.h"
#include "ufun.h"
#include "objfun.h"
#include "pobjfun.h"
#include "mapfun.h"
#include <stdlib.h>
#include <limits.h>
#include <string.h>

Player u;

static void apply_obj_bonuses(int obj)
{
    if (obj != NO_OBJ)
    {
        int po = permobj_of_obj(obj);
        u.shmax = min_int16(MAX_PLAYER_SH, u.shmax + permobjs[po].power.shield);
        u.shield_regen += min_int16(MAX_PLAYER_SHIELD_REGEN, u.shield_regen + permobjs[po].power.shield_regen);
        u.evasion += permobjs[po].power.evasion;
        u.tohit += permobjs[po].power.tohit;
    }
}

void recalc_derived(void)
{
    u.shield_regen = BASE_PLAYER_SHIELD_REGEN;
    u.hpmax = min_int16(BASE_PLAYER_HP + (u.level * PLAYER_HP_PER_LVL), MAX_PLAYER_HP);
    u.shmax = min_int16(BASE_PLAYER_SH + (u.level * PLAYER_SH_PER_LVL), MAX_PLAYER_SH);
    u.evasion = BASE_PLAYER_EVASION + (u.level * PLAYER_EVA_PER_LVL);
    u.tohit = BASE_PLAYER_TOHIT + (u.level * PLAYER_TOHIT_PER_LVL);
    apply_obj_bonuses(u.melee_weap);
    apply_obj_bonuses(u.ranged_weap);
    apply_obj_bonuses(u.armour);
    apply_obj_bonuses(u.plugin);
    status_updated = true;
    display_update();
}

int move_player(int dy, int dx)
{
    if (pos_is_oob(u.y + dy, u.x + dx))
    {
        print_msg("Attempted move out of bounds.\n");
        return 0; /* No movement. */
    }
    if ((myabs(dy) > 1) || (myabs(dx) > 1))
    {
        print_msg("Attempted to move more than one square.\n");
        return 0; /* No movement. */
    }
    if (MON_AT(u.y + dy,u.x + dx) != NO_MON)
    {
        return player_attack(dy, dx);
    }
    switch (TERR_AT(u.y + dy,u.x + dx))
    {
    case WALL:
        print_msg("You cannot go there.\n");
        return 0;
    case FLOOR:
    case DOOR:
    case STAIRS:
        reloc_player(u.y + dy, u.x + dx);
        return 1;
    }
    return 0;
}

int reloc_player(int y, int x)
{
    int y2, x2;
    u.y = y;
    u.x = x;
    if (REGION_AT(y, x) != NO_ROOM)
    {
        for (y2 = roombounds[REGION_AT(y,x)].top_y; y2 <= roombounds[REGION_AT(y,x)].bot_y; y2++)
        {
            for (x2 = roombounds[REGION_AT(y,x)].left_x; x2 <= roombounds[REGION_AT(y,x)].right_x; x2++)
            {
                FLAGS_AT(y2,x2) |= MAPFLAG_EXPLORED;
            }
        }
    }
    for (y2 = y - 1; y2 <= y + 1; y2++)
    {
        if ((y2 < 0) || (y2 >= MAP_SIZE))
        {
            continue;
        }
        for (x2 = x - 1; x2 <= x + 1; x2++)
        {
            if ((x2 < 0) || (x2 >= MAP_SIZE))
            {
                continue;
            }
            FLAGS_AT(y2,x2) |= MAPFLAG_EXPLORED;
        }
    }
    if (OBJ_AT(y,x) != NO_OBJ)
    {
        print_msg("You see here ");
        print_obj_name(ptr_from_obj(OBJ_AT(y,x)));
        print_msg(".\n");
    }
    map_updated = 1;
    status_updated = 1;
    display_update();
    return 0;
}

int damage_u(int amount, bool bypass_shield, enum death d, const char *what)
{
    if (amount < 1)
    {
        return 0;
    }
    status_updated = true;
    if (!bypass_shield)
    {
        // every hit suppresses shield regeneration
        u.shield_timer = max_int32(u.shield_timer, game_tick + SHIELD_CHIP_INTERVAL);
        // If you have at least one point of shield remaining when a shield-OK
        // attack hits you, you cannot take hit point damage from that attack.
        if (u.shcur > 0)
        {
            u.shcur -= amount;
            if (u.shcur <= 0)
            {
                u.shcur = 0;
                // shield-break suppresses shield regen for 10 turns
                u.shield_timer = max_int32(u.shield_timer, game_tick + SHIELD_BREAK_INTERVAL);
                notify_shield_broken();
            }
            return 0;
        }
    }
    u.hpcur -= amount;
    if (u.hpcur < 0)
    {
        u.hpcur = 0;
        do_death(d, what);
        return 1;
    }
    return 0;
}

void heal_u(int amount, int boost)
{
    u.hpcur += amount;
    if (u.hpcur > u.hpmax)
    {
        if (boost)
        {
            u.hpmax++;
        }
        u.hpcur = u.hpmax;
    }
    status_updated = 1;
    print_msg("You feel better.\n");
    return;
}

/*! \brief make the player dead, and notify them.
 *
 * A hypothetical future development might want to handle things like Nethack's
 * "amulet of life saving", or You Only Live Once's player character succession
 * mechanic.
 */
void do_death(enum death d, const char *what)
{
    game_finished = 1;
    notify_death(d, what);
}

/*! \brief Initialize the player character. */
void u_init(void)
{
    int i;
    int po;
    u.invpool = vivify_obj_pool(INVENTORY_SIZE) - obj_pools;
    get_player_name(u.name, PLAYER_NAME_DISPLAYED);
    u.shield_timer = 0;
    u.level = 0;
    u.melee_weap = NO_OBJ;
    u.ranged_weap = NO_OBJ;
    u.plugin = NO_OBJ;
    u.armour = NO_OBJ;
    for (i = 0; i < INVENTORY_SIZE; ++i)
    {
        u.inventory[i] = NO_OBJ;
    }
    gain_level();
    u.hpcur = u.hpmax;
    u.shcur = u.shmax;
    po = get_pobj_by_name("yo-yo");
    if (po != NO_OBJ)
    {
        i = create_obj(po, 1, true, 0, NO_POS, NO_POS);
        wield_weapon(i);
    }
    else
    {
        FATAL("%s", "pobj lookup table doesn't work yet!");
    }
    recalc_derived();
}

int lev_threshold(int level)
{
    if (level < 10)
    {
        return 20 * (1 << (level - 1));
    }
    if (level < 20)
    {
        return 10000 * (level - 10);
    }
    if (level < 30)
    {
        return 100000 * (level - 20);
    }
    return INT_MAX;
}

void gain_level(void)
{
    if (u.level < MAX_EXP_LEVEL)
    {
        u.level++;
    }
    recalc_derived();
}

void teleport_u(void)
{
    int room_try;
    int cell_try;
    int room;
    int y, x;
    for (room_try = 0; room_try < MAX_ROOMS * 4; room_try++)
    {
        room = zero_die(MAX_ROOMS);
        for (cell_try = 0; cell_try < 200; cell_try++)
        {
            y = exclusive_flat(roombounds[room].top_y, roombounds[room].bot_y);
            x = exclusive_flat(roombounds[room].left_x, roombounds[room].right_x);
            if ((MON_AT(y,x) == NO_MON) && (TERR_AT(y,x) == FLOOR))
            {
                print_msg("You are whisked away!\n");
                reloc_player(y, x);
                return;
            }
        }
    }
    print_msg("You feel briefly dislocated.\n");
    return;
}

int get_free_inventory_slot(void)
{
    int i;
    for (i = 0; i < INVENTORY_SIZE; ++i)
    {
        if (!inv_slot_occupied(i))
        {
            return i;
        }
    }
    return NO_SLOT;
}
extern bool tile_in_sight(int32_t y, int32_t x)
{
    int ady = y - u.y;
    int adx = x - u.x;
    if (ady < 0)
    {
        ady = -ady;
    }
    if (adx < 0)
    {
        adx = -adx;
    }
    return ((ady < 2) && (adx < 2)) || ((REGION_AT(y,x) != NO_ROOM) && (REGION_AT(u.y, u.x) == REGION_AT(y,x)));
}

void boost_shield(int16_t amount)
{
    if (u.shcur < MAX_PLAYER_SH)
    {
        amount = min_int16(amount, MAX_PLAYER_SH - u.shcur);
        u.shcur += amount;
        status_updated = true;
        display_update();
    }
}

void restore_shield(int16_t amount)
{
    if (u.shcur < u.shmax)
    {
        amount = min_int16(amount, u.shmax - u.shcur);
        u.shcur += amount;
        status_updated = true;
        display_update();
    }
}

void bookkeep_u(void)
{
    if (game_tick > u.shield_timer)
    {
        if (u.shcur < u.shmax)
        {
            restore_shield(u.shield_regen);
        }
        else if (u.shcur > u.shmax)
        {
            u.shcur--;
        }
    }
}

/* u.c */
