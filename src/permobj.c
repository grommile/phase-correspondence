/*! \file permobj.c
 * \brief Object database
 *
 * Permobjs contain the prototypical properties for objects.
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "central.h"

int last_populated_permobj = MAX_PERMOBJ_SLOTS - 1;

#define WEAP_STATS(dmg,secdmg,dtyp,rate,acy) { (dmg), (secdmg), (dtyp), (rate), 0, 0, 0, (acy), EFFECT_nil, 0 }
#define ARMR_STATS(shi,reg,eva,acy) { 0, 0, DT_PHYS, 0, (shi), (reg), (eva), (acy), EFFECT_nil, 0 }
#define SHIELDREST_STATS(pot) { 0, 0, DT_PHYS, 0, 0, 0, 0, 0, EFFECT_restore_shield, (pot) }
#define SHIELDBOOST_STATS(pot) { 0, 0, DT_PHYS, 0, 0, 0, 0, 0, EFFECT_boost_shield, (pot) }
#define MONSENSE_STATS(pot) { 0, 0, DT_PHYS, 0, 0, 0, 0, 0, EFFECT_monster_sense, (pot) }
#define HEAL_STATS(pot) { 0, 0, DT_PHYS, 0, 0, 0, 0, 0, EFFECT_restore_health, (pot) }
#define USELESS_STATS { 0, 0, DT_PHYS, 0, 0, 0, 0, 0, EFFECT_nil, 0 }

/*! \brief the object database */
Permobj const permobjs[MAX_PERMOBJ_SLOTS] = {
    {
        true, "dollar", "dollars", '$', Gcol_green,
        "Physical currency denominated in Federation Dollars.",
        POCLASS_SHINIES, 0, 0, 1,
        USELESS_STATS,
        { POF0_currency, 0 }
    },
    {
        true, "yo-yo", "yo-yos", ')', Gcol_dark_yellow,
        "A yo-yo, so that yo can yo.",
        POCLASS_WEAPON, 1, 0, 50,
        WEAP_STATS(1, 0, DT_PHYS, 1, 0),
        { 0, 0 }
    }
};

Permobj_stat permobj_stats[MAX_PERMOBJ_SLOTS];

