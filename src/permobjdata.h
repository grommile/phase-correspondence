/*! \file permobjdata.h
 * \brief permobj struct header file for Phase Correspondence
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

/* This header is included by central.h, but files shouldn't need to have a
 * Makefile dependency on it unless they actually manipulate permobjs. */

/*! \brief lookup numbers for effects of useable items */
typedef enum use_effect {
    EFFECT_nil = -1,
    EFFECT_restore_shield, //!< restores potency%, rounded up
    EFFECT_boost_shield, //!< +potency; ignores shield cap; xs decay 1/turn
    EFFECT_restore_health, //!< restores potency%, rounded up
    EFFECT_monster_sense, //!< sees all nearby monsters; lasts potency ticks
} Use_effect;

/*! \brief General category of a permobj */
enum poclass_num {
    POCLASS_NONE = 0, POCLASS_WEAPON = 1, POCLASS_ARMOUR = 2,
    POCLASS_PLUGIN = 3, POCLASS_CONSUMABLE = 4, POCLASS_SHINIES = 5
};

/*! \brief structure containing functional properties of permobjs */
typedef struct objpower {
    int32_t damage; //!< Damage per attack
    int32_t sec_dmg; //!< secondary damage per attack
    Damtyp sec_dtyp; //!< type of secondary damage
    int32_t atk_rate; //!< Attacks per attack action
    int32_t shield; //!< Bonus to max shields
    int32_t shield_regen; //!< Bonus to shield regen
    int32_t evasion; //!< Bonus to evasion percentage
    int32_t tohit; //!< Bonus to attack accuracy
    Use_effect effect; //!< Effect index for consumables
    int32_t potency; //!< Potency of consumable effect
} Objpower;

/*! \brief number of uint32_t to dedicate to permon flags */
enum {
    POBJ_FLAG_FIELDS = 2
};

/*! \brief flag bits for permobj flags field 0 */
enum POFLAGS_0 {
    POF0_easyknown  = 0x00000002u, /*!< is always known */
    POF0_ranged     = 0x00000100u, /*!< weapon is ranged */
    POF0_stackable  = 0x00010000u, /*!< is stackable */
    POF0_currency   = 0x00020000u, /*!< worthless scoremoniez */
    POF0_advanced   = 0x01000000u, /*!< advanced technology */
    POF0_magical    = 0x02000000u, /*!< magical */
};

/*! \brief static data about a type of object */
typedef struct permobj {
    bool valid; /*!< validity flag */
    const char name_en[32]; /*!< English-language name, used as lookup key */
    const char plural_en[32]; /*!< English-language plural. */
    char sym; /*!< Terminal display symbol */
    Game_colour colour; /*!< Terminal display colour in orthospace */
    char const *description_en; /*!< English-language descriptive text. */
    enum poclass_num poclass; /*!< Class of permobj. */
    int32_t mindepth; /*!< Minimum creation depth. */
    int32_t rarity; /*!< Chance in 100 of being thrown away and regen'd. */
    uint32_t dollarval;
    Objpower power;
    uint32_t flags[POBJ_FLAG_FIELDS];
} Permobj;

/*! \brief Statistics about a permobj */
typedef struct permobj_stat {
    uint32_t num_generated;
    bool identified;
    int16_t flavour; /*!< only applicable to randomized types */
} Permobj_stat;

/*! \brief the value designating no permobj or a random permobj */
#define NO_POBJ (-1)

GLOBALDATA Permobj const permobjs[MAX_PERMOBJ_SLOTS];
GLOBALDATA Permobj_stat permobj_stats[MAX_PERMOBJ_SLOTS];
GLOBALDATA int last_populated_permobj;

/* permobjdata.h */
