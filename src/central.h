/*! \file central.h
 * \brief main header file for Phase Correspondence
 */

/* Copyright © 2005, 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#ifndef INC_central_h
#define INC_central_h

#define PUREFUNC __attribute__((pure))
#define CONSTFUNC __attribute__((const))
#define MYUNUSED __attribute((unused))

#ifdef __cplusplus
#define INLINEFUNC inline
#define GLOBALFUNC extern "C"
#define GLOBALDATA extern
#else
#define INLINEFUNC static inline
#define GLOBALFUNC extern
#define GLOBALDATA extern
#endif

#include <stdint.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdlib.h>
#include <limits.h>
#include <stdnoreturn.h>
#include <assert.h>
#include "config.h"
#include "consts.h"
#include "util.h"

typedef int8_t Tinynum;
typedef int16_t Damagenum;
typedef int16_t Accuracynum;
typedef int32_t Gametimenum;
typedef int32_t Objnum;

#include "permondata.h"
#include "permobjdata.h"
#include "objdata.h"
#include "mondata.h"
#include "playerdata.h"
#include "mapdata.h"

/*! \brief Detailed representation of a game command */
typedef struct full_cmd {
    Game_cmd cmd;
    intptr_t extra;
} Full_cmd;

/*! \brief Representation of a direction on the grid. 
 *
 * On screen, positive y = down, positive x = right. */
typedef struct direction {
    int32_t y;
    int32_t x;
} Direction;

typedef bool (*Inv_filter) (int obj, intptr_t extra);

typedef struct i32list {
    int len;
    int32_t *list;
} I32list;

GLOBALDATA bool game_finished;
GLOBALDATA bool game_started;
GLOBALDATA int32_t game_tick;

GLOBALDATA bool status_updated;
GLOBALDATA bool map_updated;
GLOBALDATA bool display_initialized;

GLOBALFUNC void print_msg(const char *fmt, ...);
GLOBALFUNC void vprint_msg(char const *fmt, va_list ap);
GLOBALFUNC int display_init(void);
GLOBALFUNC void display_update(void);
GLOBALFUNC int display_shutdown(bool skip_prompt);
GLOBALFUNC bool get_command(Full_cmd *dest);
GLOBALFUNC int get_player_name(char *buf, int max_printables);

/* config file handling */
GLOBALFUNC void load_config(void);
GLOBALFUNC char const *get_config(char const *key);
GLOBALFUNC int get_config_as_integer(char const *key, int32_t *destptr);

/* various notification messages
 *
 * This is an effort to move print_msg() calls *out* of random bits and pieces
 * of objects.c, monsters.c, etc. */
GLOBALFUNC void notify_used_last(int po);
GLOBALFUNC void notify_wield(int obj);
GLOBALFUNC void notify_wear(int obj);
GLOBALFUNC void notify_plug_in(int obj);
GLOBALFUNC void notify_unwield(int obj);
GLOBALFUNC void notify_ready_ranged(int obj);
GLOBALFUNC void notify_take_off(int obj);
GLOBALFUNC void notify_remove(int obj);
GLOBALFUNC void notify_use_useless(void);
GLOBALFUNC void notify_shield_broken(void);
GLOBALFUNC void notify_ukillm(Mon const *mptr);
GLOBALFUNC void notify_mkillm(Mon const *er, Mon const *ee);
GLOBALFUNC void notify_mondie(Mon const *mptr);
GLOBALFUNC void notify_death(Death d, char const *what);
GLOBALFUNC void notify_now_have(int slot);
GLOBALFUNC void notify_get(int obj);
GLOBALFUNC void notify_currency_gain(int32_t amt);
GLOBALFUNC void notify_pack_full(void);
GLOBALFUNC void notify_cant_unwield(void);
GLOBALFUNC void notify_drop(Obj const *optr);
GLOBALFUNC void notify_drop_blocked(void);
GLOBALFUNC void notify_mon_wakes(Mon const *mptr);
GLOBALFUNC void notify_umiss(Mon const *mptr);
GLOBALFUNC void notify_mmissu(Mon const *mptr);
GLOBALFUNC void notify_mmissm(Mon const *er, Mon const *ee);
GLOBALFUNC void notify_uhitm(Mon const *mptr, int16_t amt);
GLOBALFUNC void notify_mhitu(Mon const *mptr, int16_t amt);

/* ISO 9899:2011's "noreturn" specifier cannot be applied to function pointers.
 * However, it should be clearly indicated to the compiler that calling
 * the fatal() function pointer will not return. Hence the following macro. */
#if defined(__GNUC__)
#define NORETURN_FUNCPTR __attribute__((__noreturn__))
#else
#define NORETURN_FUNCPTR
#endif
GLOBALFUNC int exclusive_flat(int lower, int upper); /* l+1 ... u-1 */
GLOBALFUNC int inclusive_flat(int lower, int upper); /* l ... u */
GLOBALFUNC int one_die(int sides);  /* 1..n */
GLOBALFUNC int dice(int count, int sides);
GLOBALFUNC int zero_die(int sides); /* 0..n-1 */
GLOBALFUNC void dice_init(bool use_strong);
GLOBALFUNC void save_rng_state(void);
GLOBALDATA void NORETURN_FUNCPTR (*fatal)(char const *file, int line, char const *fmt, ...);
#define FATAL(fmt, ...) fatal(__FILE__, __LINE__, (fmt), __VA_ARGS__)
GLOBALFUNC void bug(char const *file, int line, char const *fmt, ...);
#define BUG(fmt, ...) bug(__FILE__, __LINE__, (fmt), __VA_ARGS__)
GLOBALFUNC void notice(char const *file, int line, char const *fmt, ...);
#define NOTICE(fmt, ...) notice(__FILE__, __LINE__, (fmt), __VA_ARGS__)

/*! \brief Initialize a size-checked array of 32-bit integers */
INLINEFUNC void i32list_init(I32list *list)
{
    list->len = 0;
    list->list = NULL;
}

/*! \brief Append an entry to a size-checked array of 32-bit integers, no checks */
INLINEFUNC void i32list_append_uc(I32list *list, int32_t i)
{
    list->len++;
    list->list = (int32_t *) realloc(list->list, list->len * sizeof (int32_t));
    list->list[list->len - 1] = i;
}

/*! \brief a random percentage value for testing N-percent chances */
INLINEFUNC int percentile(void) { return zero_die(100); }

/*! \brief a random percentage value from nothing to everything */
INLINEFUNC int full_percent(void) { return zero_die(101); }

/*! \brief an unweighted coin toss */
INLINEFUNC bool coinflip(void) { return zero_die(2); }

extern bool tile_in_sight(int32_t y, int32_t x) PUREFUNC;

/*! \brief lower of two 16-bit integers */
INLINEFUNC int16_t min_int16(int16_t l, int16_t r)
{
    return (l < r) ? l : r;
}

/*! \brief higher of two 16-bit integers */
INLINEFUNC int16_t max_int16(int16_t l, int16_t r)
{
    return (l > r) ? l : r;
}

/*! \brief lower of two 32-bit integers */
INLINEFUNC int32_t min_int32(int32_t l, int32_t r)
{
    return (l < r) ? l : r;
}

/*! \brief higher of two 32-bit integers */
INLINEFUNC int32_t max_int32(int32_t l, int32_t r)
{
    return (l > r) ? l : r;
}

/*! \brief lower of two 64-bit integers */
INLINEFUNC int64_t min_int64(int64_t l, int64_t r)
{
    return (l < r) ? l : r;
}

/*! \brief higher of two 64-bit integers */
INLINEFUNC int64_t max_int64(int64_t l, int64_t r)
{
    return (l > r) ? l : r;
}

/*! \brief lower of two integers */
INLINEFUNC int min_int(int l, int r)
{
    return (l < r) ? l : r;
}

/*! \brief higher of two integers */
INLINEFUNC int max_int(int l, int r)
{
    return (l > r) ? l : r;
}

/*! \brief sign of an integer */
INLINEFUNC int mysign(int a)
{
    return (a > 0) ? 1 : ((a < 0) ? -1 : 0);
}

/*! \brief absolute value of a signed integer */
INLINEFUNC int myabs(int a)
{
    return (a >= 0) ? a : -a;
}

INLINEFUNC int geom_dist_from_orthogonal(int dy, int dx) CONSTFUNC;
INLINEFUNC int geom_dist_from_diagonal(int dy, int dx) CONSTFUNC;

/*! \brief distance of (dy,dx) from an orthogonal */
INLINEFUNC int geom_dist_from_orthogonal(int dy, int dx)
{
    return min_int(myabs(dy), myabs(dx));
}

/*! \brief distance of (dy,dx) from a diagonal */
INLINEFUNC int geom_dist_from_diagonal(int dy, int dx)
{
    return myabs(myabs(dy) - myabs(dx));
}

INLINEFUNC bool geom_is_cardinal(int dy, int dx)
{
    return (dy == 0) || (dx == 0) || (myabs(dy) == myabs(dx));
}

GLOBALFUNC int geom_towards_diagonal(int dy, int dx, int *restrict new_dy, int *restrict new_dx, int max_points);
GLOBALFUNC int geom_towards_goal(int dy, int dx, int *restrict new_dy, int *restrict new_dx, int max_points);
GLOBALFUNC int geom_towards_orthogonal(int dy, int dx, int *restrict new_dy, int *restrict new_dx, int max_points);
GLOBALFUNC int geom_flee_goal(int dy, int dx, int *restrict new_dy, int *restrict new_dx, int max_points);

/*! \brief Validity check of a coordinate pair */
INLINEFUNC bool pos_is_nowhere(int32_t y, int32_t x)
{
    return (y == NO_POS) || (x == NO_POS);
}

/*! \brief bounds check of a coordinate pair */
INLINEFUNC bool pos_is_oob(int32_t y, int32_t x)
{
    return (y < dun_min_y) || (x < dun_min_x) || (y > dun_max_y) || (x >= dun_max_x);
}

/*! \brief magnitude of a coordinate pair under chebyshev */
INLINEFUNC int32_t cheby_magnitude(int32_t y, int32_t x)
{
    return max_int32(myabs(y), myabs(x));
}

#endif

/* central.h */
