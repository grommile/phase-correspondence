/*! \file util.c
 * \brief Miscellaneous pure-C utility functions
 *
 * The functions in this file provide "back-end" functionality such as
 * building application directory paths, creating multi-element directory
 * paths that don't exist, etc.
 *
 * At present, if this file is fully functional on Windows, that is a case
 * of working "by accident". I entirely expect it to do surprising things on
 * Windows, and welcome patches (within reason) to fix it. At some point I
 * might set up my own Windows build environment and try to iron things out.
 *
 * \todo Do the necessary wrangling to make them play nicely on Windows
 */

/* Copyright © 2014-2016 Martin Read
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 */

#include "util.h"
/* On Linux, define _GNU_SOURCE. */
#ifdef __linux__
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <basedir.h>
#include <ctype.h>
#include <limits.h>

#undef UTIL_DEBUG
//#define UTIL_DEBUG

/*! \brief Path separator character */
#define GROMMILE_PATHSEP_CHAR '/'

/*! \brief String containing the separator character */
#define GROMMILE_PATHSEP_STRING "/"

static char *app_suffix(char const *vendor, char const *app);

/*! \brief Check whether a specified path points to an existing directory.
 *
 * \return -1 if path does not point to a directory, 0 otherwise
 * \param path Path to check
 */
int grommile_verify_dir_available(char const *path)
{
    struct stat s;
    int i;
    if (!path || !*path)
    {
        errno = EINVAL;
        return -1;
    }
    i = stat(path, &s);
    if (i != 0)
    {
        return -1;
    }
    if ((s.st_mode & S_IFMT) != S_IFDIR)
    {
        errno = ENOTDIR;
        return -1;
    }
    return 0;
}

/*! \brief module-wide handle obtained from libxdg-basedir */
static xdgHandle grommile_xdg_handle;

/*! \brief base directory for per-application data storage */
static char const *grommile_data_home;

/*! \brief base directory for per-application configuration storage */
static char const *grommile_config_home;

/*! \brief per-application directory for data storage */
static char *grommile_data_dir;

/*! \brief per-application directory for configuration storage */
static char *grommile_config_dir;

/*! \brief per-application substring for building data and config paths */
static char *grommile_app_subdir;

/*! \brief directory handle for data dir */
static DIR *grommile_data_dp;

/*! \brief directory handle for config dir */
static DIR *grommile_config_dp;

/*! \brief list of bytes to exclude during string sanitization
 *
 * This list includes:
 *
 * 1. double quote, single quote, and backtick (quoting characters)
 * 2. semicolon (command terminator/separator)
 * 3. ampersand (run-in-background marker)
 * 4. less-than, greater-than, pipe (I/O redirection operators)
 * 5. colon (drive letter separator)
 * 6. dollar (POSIX shell variable expansion operator)
 * 7. percent (MS environment variable expansion operator)
 * 8. newline and carriage return
 * 9. slash (POSIX path separator)
 * 10. backslash (MS path separator)
 */
char const *grommile_HYGIENIC_STRING = "`\"\\'/<>:&;$%|\n\r";

/*! \brief Set up directory access for config and data files
 *
 * This function uses libxdg-basedir to obtain the XDG-specified configuration
 * and data home directories, then autovivifies app-specific subdirectories
 * within those directories.
 *
 * \return -1 if any errors are encountered, otherwise zero.
 * \param vendor "Vendor name" for building directory suffix
 * \param app "Application name" for building directory suffix
 * \param data_fd Location to store data directory fd
 * \param config_fd Location to store config directory fd
 */
int grommile_setup_dirs(char const *vendor, char const *app, int *data_fd, int *config_fd)
{
    int i;
    xdgHandle *x;
    if (!data_fd || !config_fd)
    {
        errno = EINVAL;
        return -1;
    }
    if (!grommile_data_dp || !grommile_config_dp)
    {
        x = xdgInitHandle(&grommile_xdg_handle);
        if (!x)
        {
            return -1;
        }
        if (!grommile_app_subdir)
        {
            if (!vendor || !app || !*vendor || !*app)
            {
                errno = EINVAL;
                return -1;
            }
            grommile_app_subdir = app_suffix(vendor, app);
            if (!grommile_app_subdir)
            {
                return -1;
            }
        }
        if (!grommile_data_home)
        {
            grommile_data_home = xdgDataHome(x);
            if (!grommile_data_home)
            {
                return -1;
            }
        }
        if (!grommile_config_home)
        {
            grommile_config_home = xdgConfigHome(x);
            if (!grommile_config_home)
            {
                return -1;
            }
        }
        if (!grommile_data_dir)
        {
            grommile_data_dir = grommile_strbuild(grommile_data_home, grommile_app_subdir);
            if (!grommile_data_dir)
            {
                return -1;
            }
        }
        if (!grommile_config_dir)
        {
            grommile_config_dir = grommile_strbuild(grommile_config_home, grommile_app_subdir);
            if (!grommile_config_dir)
            {
                return -1;
            }
        }
        if (!grommile_config_dp)
        {
            i = grommile_verify_dir_available(grommile_config_dir);
            if (i != 0)
            {
                if (errno == ENOENT)
                {
                    i = grommile_makepath(grommile_config_dir, S_IRWXU);
                    if (i != 0)
                    {
                        return -1;
                    }
                }
            }
            grommile_config_dp = opendir(grommile_config_dir);
            if (!grommile_config_dp)
            {
                return -1;
            }
        }
        if (!grommile_data_dp)
        {
            i = grommile_verify_dir_available(grommile_data_dir);
            if (i != 0)
            {
                if (errno == ENOENT)
                {
                    i = grommile_makepath(grommile_data_dir, S_IRWXU);
                    if (i != 0)
                    {
                        return -1;
                    }
                }
            }
            grommile_data_dp = opendir(grommile_data_dir);
            if (!grommile_data_dp)
            {
                return -1;
            }
        }
    }
    *data_fd = dirfd(grommile_data_dp);
    *config_fd = dirfd(grommile_config_dp);
    return 0;
}

/*! \brief Create a C string that is a concatenation of two existing strings
 *
 * The string returned by this function has been allocated with malloc() and
 * should be released with free() when it is no longer required.
 *
 * \return Pointer to a malloc()-allocated buffer containing the resulting string
 * \param left The string which should appear first in the output
 * \param right The string which should appear second in the output
 */
char *grommile_strbuild(char const *left, char const *right)
{
    if (!left || !right)
    {
        return NULL;
    }
    size_t s1 = strlen(left);
    size_t s2 = strlen(right);
    size_t s = s1 + s2 + 1;
    char *str = (char *) malloc(s);
    if (str)
    {
        memcpy(str, left, s1);
        memcpy(str + s1, right, s2);
    }
    str[s1 + s2] = '\0';
    return str;
}

/*! \brief Iteratively create the directories of a given path.
 *
 * This function exists because there is no POSIX (or Linux) syscall that does
 * the equivalent of system("mkdir -p \"my/path/here\"") - which is really
 * unfortunate, because this means that creating such a directory structure
 * is inherently racy.
 *
 * \return return value of last mkdir call, or -1 if path is NULL.
 * \param path Directory path to build
 * \param mode Filesystem permissions to set on the directories being created
 *
 * \todo Implement a Windows-friendly variant.
 */
int grommile_makepath(char const *path, int mode)
{
    if (!path)
    {
        errno = EINVAL;
        return -1;
    }
#ifdef UTIL_DEBUG
    fprintf(stderr, "grommile_makepath(\"%s\", %d)\n", path, mode);
#endif
    char *realpath = strdup(path);
    char *pathtrak = ((realpath[0] == GROMMILE_PATHSEP_CHAR) ? realpath + 1 : realpath);
    int rv = -1;
    do
    {
        struct stat s;
        int i;
        pathtrak = strchr(pathtrak, GROMMILE_PATHSEP_CHAR);
        if (pathtrak)
        {
            pathtrak[0] = '\0';
#ifdef UTIL_DEBUG
            fprintf(stderr, "%d Checking %s (pathtrak offset %d)\n", __LINE__, realpath, (int) (pathtrak - realpath));
#endif
            i = stat(realpath, &s);
            if (i == 0)
            {
#ifdef UTIL_DEBUG
                fprintf(stderr, "%d stat(\"%s\") returned 0\n", __LINE__, realpath);
#endif
                if ((s.st_mode & S_IFMT) == S_IFDIR)
                {
                    pathtrak[0] = GROMMILE_PATHSEP_CHAR;
                    ++pathtrak;
                }
                else
                {
#ifdef UTIL_DEBUG
                    fprintf(stderr, "%d not a directory\n", __LINE__);
#endif
                    errno = ENOTDIR;
                    rv = -1;
                    break;
                }
            }
            else
            {
#ifdef UTIL_DEBUG
                fprintf(stderr, "%d stat(\"%s\") returned %d, errno %d (%s)\n",
                        __LINE__, realpath, i, errno, strerror(errno));
#endif
                if (errno == ENOENT)
                {
#ifdef UTIL_DEBUG
                    fprintf(stderr, "%d mkdir(\"%s\", %d)\n", __LINE__, realpath, mode);
#endif
                    i = mkdir(realpath, mode);
                    if (i != 0)
                    {
#ifdef UTIL_DEBUG
                        fprintf(stderr, "%d mkdir(\"%s\", %d) returned %d, errno %d (%s)\n", __LINE__, realpath, mode, i, errno, strerror(errno));
#endif
                        rv = -1;
                        break;
                    }
                    pathtrak[0] = GROMMILE_PATHSEP_CHAR;
                    ++pathtrak;
                }
            }
        }
        else
        {
            i = stat(realpath, &s);
            if (i == 0)
            {
#ifdef UTIL_DEBUG
                fprintf(stderr, "%d stat(\"%s\") returned 0\n", __LINE__, realpath);
#endif
                if ((s.st_mode & S_IFMT) == S_IFDIR)
                {
                    rv = 0;
                }
                else
                {
#ifdef UTIL_DEBUG
                    fprintf(stderr, "not a directory\n");
#endif
                    errno = ENOTDIR;
                    rv = -1;
                }
            }
            else
            {
                if (errno == ENOENT)
                {
#ifdef UTIL_DEBUG
                    fprintf(stderr, "%d mkdir(\"%s\", %d)\n", __LINE__, realpath, mode);
#endif
                    i = mkdir(realpath, mode);
                    if (i != 0)
                    {
#ifdef UTIL_DEBUG
                        fprintf(stderr, "%d mkdir(\"%s\", %d) returned %d, errno %d (%s)\n", __LINE__, realpath, mode, i, errno, strerror(errno));
#endif
                        rv = -1;
                    }
                    else
                    {
#ifdef UTIL_DEBUG
                        fprintf(stderr, "%d mkdir(\"%s\", %d) returned 0\n", __LINE__, realpath, mode);
#endif
                        rv = 0;
                    }
                }
            }
        }
    } while (pathtrak);
    free(realpath);
    return rv;
}

/*! \brief Verify whether a character is permissible at start-of-name
 *
 * Unacceptable names include not only those names which contain characters in
 * grommile_HYGIENIC_STRING, but also those names which start with minus (which
 * as the first character of a command-line argument habitually designates an
 * *option* in POSIX-like operating systems) or a space character (visually
 * confusing).
 *
 * Forbidding octets with the high bit set is superficially attractive but
 * on closer examination is self-evidently exclusionary.
 *
 * \return true if character is forbidden, false otherwise
 * \param c Character to check
 * \todo Make the places that use this support the necessary nuances to handle Unicode in a sane fashion
 */
static bool forbidden_first_char(unsigned char c)
{
    return (c == '-') || isspace(c) || (strchr(grommile_HYGIENIC_STRING, c));
}

/*! \brief compose a directory suffix from a vendor name and an app name
 *
 * If the allocation fails or the provided strings do not meet the
 * requirements, this function returns NULL.
 *
 * The requirements on the vendor and app name are:
 *
 * 1. They must not be NULL or the empty string.
 * 2. The characters in grommile_HYGIENIC_STRING must not appear (for various reasons)
 * 3. The first character must not be - or SPC.
 *
 * The pointer returned by this function points to a buffer allocated using
 * malloc(), and should be manually free()'d when no longer required.
 *
 * \return pointer to malloc-allocated result string
 * \param vendor "Vendor name" to use in suffix
 * \param app "Application name" to use in suffix
 */
static char *app_suffix(char const *vendor, char const *app)
{
    size_t s1;
    size_t s2;
    size_t s3;
    size_t s4;
    char *str;
    if (!vendor || !*vendor || !app || !*app)
    {
        errno = EINVAL;
        return NULL;
    }
    if (forbidden_first_char(*vendor) || forbidden_first_char(*app))
    {
        errno = EINVAL;
        return NULL;
    }
    s1 = strlen(vendor);
    if (strcspn(vendor, grommile_HYGIENIC_STRING) != s1)
    {
        errno = EINVAL;
        return NULL;
    }
    s3 = strlen(app);
    if (strcspn(app, grommile_HYGIENIC_STRING) != s3)
    {
        errno = EINVAL;
        return NULL;
    }
    s2 = strlen(GROMMILE_PATHSEP_STRING);
    s4 = 1 + s3 + 2*s2 + s1;
    str = (char *) malloc(s4);
    if (str)
    {
        memcpy(str, GROMMILE_PATHSEP_STRING, s2);
        memcpy(str + s2, vendor, s1);
        memcpy(str + s1 + s2, GROMMILE_PATHSEP_STRING, s2);
        memcpy(str + s1 + 2*s2, app, s3);
    }
    return str;
}

/*! \brief Hash up to four bytes of a string to a single byte
 *
 * This is a very simplistic hashing function that generates a one-byte
 * hash value from a string.
 *
 * \param s String to hash.
 */
uint8_t grommile_string_bytehash(char const *s)
{
    uint8_t val = 0;
    int i;
    for (i = 0; i < 4; ++i)
    {
        val = (((val & 0xfu) << 4) | ((val & 0xf0u) >> 4)) + (uint8_t) *s;
        if (*s) ++s;
    }
    return val;
}

/*! \brief Hash up to four bytes of a string to a 16-bit integer
 *
 * This is a very simplistic hashing function that generates a two-byte
 * hash value from a string.
 *
 * \param s String to hash.
 */
uint16_t grommile_string_shorthash(char const *s)
{
    uint16_t val = 0;
    int i;
    for (i = 0; i < 4; ++i)
    {
        val = (((val & 0xffu) << 8) | ((val & 0xff00u) >> 8)) + (uint8_t) *s;
        if (*s) ++s;
    }
    return val;
}

static int32_t grommile_search_bucket(struct grommile_kvbucket *bucket, char const *key)
{
    int32_t i;
    if (!bucket || !key)
    {
        return -1;
    }
    for (i = 0; i < bucket->size; ++i)
    {
        if (!strcmp(bucket->data[i].key, key))
        {
            return i;
        }
    }
    return -1;
}

static int grommile_add_kv(struct grommile_kvbucket *bucket, char const *key, char const *value);
static int grommile_replace_value(struct grommile_kvbucket *bucket, int32_t idx, char const *value);

/*! \brief Replace a value in a grommile_kvbucket
 *
 * \param bucket Pointer to the bucket
 * \param idx Index of the key/value pair whose value should be replaced
 * \param value New value to store for the k/v pair
 */
static int grommile_replace_value(struct grommile_kvbucket *bucket, int32_t idx, char const *value)
{
    char *tmp = strdup(value);
    if (!tmp)
    {
        return -1;
    }
    free(bucket->data[idx].value);
    bucket->data[idx].value = tmp;
    return 0;
}

/*! \brief Add a new value to a key/value bucket
 *
 * If this function returns -1 indicating an error, `errno` may have any value
 * permitted for `strdup()` or `realloc()`. Alternatively, the following values
 * may arise directly:
 *
 * Value  | Meaning
 * ------ | -------------------------
 * ENOMEM | Expanding the bucket's size params would cause integer overflow
 * EINVAL | Any parameter was NULL, the key string was empty, or the bucket's size/reservation fields were corrupt.
 *
 * \param bucket Pointer to the bucket
 * \param key Key to append to the bucket
 * \param value Value to associate with the key
 * \return -1 if fail, 0 if success
 */
static int grommile_add_kv(struct grommile_kvbucket *bucket, char const *key, char const *value)
{
    if (!bucket || !key || !value || !*key)
    {
        // NULL parameters or empty key string
        errno = EINVAL;
        return -1;
    }
    if ((bucket->size < 0) || (bucket->reservation < 0) ||
        (bucket->size > bucket->reservation))
    {
        // malformed bucket
        errno = EINVAL;
        return -1;
    }
    if ((bucket->size == INT_MAX) ||
        ((bucket->size == bucket->reservation) &&
         (bucket->reservation > (INT_MAX >> 1))))
    {
        errno = ENOMEM;
        return -1;
    }
    bucket->size++;
    if (bucket->size > bucket->reservation)
    {
        struct grommile_kvpair *tmp_ptr =
            (struct grommile_kvpair *) realloc(bucket->data, ((bucket->reservation << 1) * sizeof (struct grommile_kvpair)));
        if (!tmp_ptr)
        {
            return -1;
        }
        bucket->data = tmp_ptr;
        bucket->reservation <<= 1;
    }
    char *keydup = NULL;
    char *valdup = NULL;
    keydup = strdup(key);
    if (!keydup)
    {
        goto fail;
    }
    valdup = strdup(value);
    if (!valdup)
    {
        goto fail;
    }
    bucket->data[bucket->size - 1].key = keydup;
    bucket->data[bucket->size - 1].value = valdup;
    return 0;
fail:
    free(valdup);
    free(keydup);
    return -1;
}

int grommile_store_kv(struct grommile_kvstore *store, char const *key, char const *value)
{
    if (!store || !key || !value || !*key)
    {
        return -1;
    }
    uint8_t hash = grommile_string_bytehash(key);
    struct grommile_kvbucket *bucket = store->buckets + hash;
    int32_t idx = grommile_search_bucket(bucket, key);
    if (idx == -1)
    {
        return grommile_add_kv(bucket, key, value);
    }
    else
    {
        return grommile_replace_value(bucket, idx, value);
    }
}

/*! \brief Get the stored value for a specified key
 *
 * \return pointer to string value, or NULL if key is not found or params are invalid
 */
char const * grommile_fetch_kv(struct grommile_kvstore *store, char const *key)
{
    if (!store || !key || !*key)
    {
        errno = EINVAL;
        return NULL;
    }
    uint8_t hash = grommile_string_bytehash(key);
    int32_t i;
    for (i = 0; i < store->buckets[hash].size; ++i)
    {
        if (!strcmp(key, store->buckets[hash].data[i].key))
        {
            return store->buckets[hash].data[i].value;
        }
    }
    return NULL;
}

/*! \brief Initialize an allocated key/value store
 *
 */
int grommile_init_kvstore(struct grommile_kvstore *store, int32_t bucket_size_hint)
{
    int i;
    if (bucket_size_hint < 1)
    {
        bucket_size_hint = 1;
    }
    for (i = 0; i < 256; ++i)
    {
        store->buckets[i].size = 0;
        store->buckets[i].reservation = bucket_size_hint;
        store->buckets[i].data = (struct grommile_kvpair *) calloc(bucket_size_hint, sizeof (struct grommile_kvpair));
        if (!store->buckets[i].data)
        {
            int j;
            for (j = 0; j < i; ++j)
            {
                free(store->buckets[j].data);
            }
            return -1;
        }
    }
    return 0;
}

/*! \brief Allocate a new key/value store
 *
 * Values of bucket_size_hint less than 1 will be treated as 1.
 *
 * \param bucket_size_hint Suggested initial reservation for each bucket
 */
struct grommile_kvstore *grommile_create_kvstore(int32_t bucket_size_hint)
{
    if (bucket_size_hint < 1)
    {
        bucket_size_hint = 1;
    }
    struct grommile_kvstore *tmp = (struct grommile_kvstore *) calloc(1, sizeof (struct grommile_kvstore));
    if (tmp == NULL)
    {
        return NULL;
    }
    int i = grommile_init_kvstore(tmp, bucket_size_hint);
    if (i == -1)
    {
        int e = errno;
        free(tmp);
        errno = e;
        return NULL;
    }
    return tmp;
}

/*
 * vim:ts=8:sts=4:sw=4:expandtab:cindent
 */
/* util.c */
