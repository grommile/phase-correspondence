/*! \file permons.c
 * \brief Monster database
 *
 * Permons contain the prototypical properties for monsters.
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#define PERMONS_C
#include "central.h"

int last_populated_permon = MAX_PERMON_SLOTS - 1;

char const *faction_names_en[] = {
    "neutral", "friendly", "hostile", "betrayed"
};

/*! \brief the monster database */
Permon const permons[MAX_PERMON_SLOTS] = {
    {
        true, "blitter", "blitters", 'b', Gcol_light_red,
        0, 0,
        5, 0, 1, 0, Speed_fast,
        AI_charger,
        Faction_hostile,
        { PMF0_datamorph, 0 }
    },
    {
        true, "rookie guard", "rookie guards", 'g', Gcol_light_grey,
        0, 0,
        10, 0, 3, 0, Speed_normal,
        AI_charger,
        Faction_hostile,
        { 0, 0 }
    }
};

Permon_stat permon_stats[MAX_PERMON_SLOTS];

/* permons.c */
