/*! \file permondata.h
 * \brief permons header file for Phase Correspondence
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

/* This header is included by central.h, but files shouldn't need to have a
 * Makefile dependency on it unless they actually manipulate permons. */

/*! \brief number of uint32_t to dedicate to permon flags */
enum {
    PMON_FLAG_FIELDS = 2
};

/*! \brief flag bits for permon flags field 0 */
enum PMFLAGS_0 {
    PMF0_wallwalk   = 0x00000001u, /*!< Walks through walls */
    PMF0_undead     = 0x00010000u, /*!< From beyond the grave */
    PMF0_datamorph  = 0x00020000u, /*!< Made of data */
    PMF0_mechanical = 0x00040000u, /*!< Mechanical entity */
    PMF0_electronic = 0x00040000u, /*!< Electronic entity */
    PMF0_golem      = 0x00100000u, /*!< Magical machine */
};

/*! \brief flag bits for permon flags field 1 */
enum PMFLAGS_1 {
    PMF1_res_fire   = 0x00000001u, /*!< immune to fire damage */
    PMF1_res_cold   = 0x00000002u, /*!< immune to cold damage */
};

/*! \brief constant baseline data about a type of monster */
typedef struct permon {
    bool valid; /*!< This entry actually contains something */
    const char name_en[32]; /*!< Name of the monster in English */
    const char plural_en[32]; /*!< Plural form of the name in English */
    char sym; /*!< ASCII representation of the monster */
    Game_colour colour; /*!< terminal display colour in orthospace */
    int32_t rarity; /*!< Percentage chance of being rejected on a monster generation roll */
    int32_t power; /*!< Power level, used to govern monster generation */
    Damagenum hp; /*!< Base hit points */
    Accuracynum tohit; /*!< to-hit bonus */
    Damagenum dam; /*!< melee damage die sides */
    Accuracynum evasion; /*!< evasion bonus */
    Speed speed;
    AI_style ai;
    Faction faction;
    uint32_t flags[PMON_FLAG_FIELDS];
} Permon;

/*! \brief the value designating no permon or a random permon */
#define NO_PMON (-1)

/*! \brief Gameplay statistics about a permon */
typedef struct permon_stat {
    uint32_t num_generated;
    uint32_t killed_by_player;
    uint32_t killed_by_other;
} Permon_stat;

GLOBALDATA struct permon const permons[MAX_PERMON_SLOTS];
GLOBALDATA Permon_stat permon_stats[MAX_PERMON_SLOTS];
GLOBALDATA int last_populated_permon;

/* permondata.h */
