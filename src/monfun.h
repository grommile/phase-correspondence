/*! \file monfun.h
 * \brief monster functions header file
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#ifndef INC_monfun_h
#define INC_monfun_h

#include "central.h"

GLOBALFUNC void mon_acts(int mon);
GLOBALFUNC void death_drop(int mon);
GLOBALFUNC void print_mon_name(Mon const *mptr, Article article);
GLOBALFUNC int create_mon(int pm_idx, int depth, int y, int x);
GLOBALFUNC void summoning(int y, int x, int how_many);
GLOBALFUNC int ood(int power, int ratio) CONSTFUNC;
GLOBALFUNC int get_random_pmon(int depth);
GLOBALFUNC void damage_mon(int mon, int amount, bool by_you, int killer);
GLOBALFUNC bool mon_can_pass(int mon, int y, int x) PUREFUNC;
GLOBALFUNC void move_mon(int mon, int y, int x);
GLOBALFUNC void select_space(int * restrict py, int * restrict px, int dy, int dx, AI_style style);

GLOBALFUNC void mon_init(void);

GLOBALFUNC int mhitu(int mon);
GLOBALFUNC int mshootu(int mon, enum damtyp dtype);

INLINEFUNC Mon const *ptr_from_mon(int mon)
{
    return (mon == NO_MON) ? NULL : monsters + mon;
}

INLINEFUNC Mon *varptr_from_mon(int mon)
{
    return (mon == NO_MON) ? NULL : monsters + mon;
}

INLINEFUNC int permon_of_mon(int mon)
{
    return (mon == NO_MON) ? NO_PMON : monsters[mon].mon_id;
}

#endif

/* monfun.h */
