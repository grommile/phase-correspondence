/*! \file ufun.h
 * \brief player functions header file
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#include "central.h"

GLOBALFUNC void u_init(void);
GLOBALFUNC void do_death(enum death d, const char *what);
GLOBALFUNC void heal_u(int amount, int boost);
GLOBALFUNC int damage_u(int amount, bool bypass_shield, enum death d, const char *what);
GLOBALFUNC int lev_threshold(int level) CONSTFUNC;
GLOBALFUNC int move_player(int dy, int dx);
GLOBALFUNC int reloc_player(int y, int x);
GLOBALFUNC void recalc_derived(void);
GLOBALFUNC void teleport_u(void);
GLOBALFUNC int get_free_inventory_slot(void);
GLOBALFUNC void bookkeep_u(void);
GLOBALFUNC void restore_shield(int16_t amount);
GLOBALFUNC void boost_shield(int16_t amount);
GLOBALFUNC void gain_level(void);
GLOBALFUNC int player_attack(int dy, int dx);
GLOBALFUNC int uhitm(int mon);
GLOBALFUNC int ushootm(int sy, int sx);

INLINEFUNC bool inv_slot_occupied(int slot)
{
    return (u.inventory[slot] != NO_OBJ);
}

/* ufun.h */
