/*! \file consts.h
 * \brief symbolic constants for Phase Correspondence
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

/*! \brief Size limits for various pools */
enum array_sizes
{
    PLAYER_NAME_DISPLAYED = 16,
    INVENTORY_SIZE = 19,
    MAX_PERMOBJ_SLOTS = 100,
    MAX_PERMON_SLOTS = 100,
    MAX_OBJS_IN_PLAY = 100,
    MAX_MONS_IN_PLAY = 100,
    MAX_FLAVOURS = 20
};

/*! \brief Colours recognized by the game */
typedef enum game_colours
{
    Gcol_red = 1,
    Gcol_green = 2,
    Gcol_dark_yellow = 3,
    Gcol_blue = 4,
    Gcol_magenta = 5,
    Gcol_cyan = 6,
    Gcol_light_grey = 7,
    Gcol_dark_grey = 8,
    Gcol_light_red = 9,
    Gcol_light_green = 10,
    Gcol_yellow = 11,
    Gcol_light_blue = 12,
    Gcol_light_magenta = 13,
    Gcol_light_cyan = 14,
    Gcol_white = 15
} Game_colour;

/*! \brief Damage type of an attack or resistance */
typedef enum damtyp {
    DT_PHYS = 0, DT_COLD, DT_FIRE, DT_NECRO, DT_ELEC
} Damtyp;

/*! \brief time constants for the player */
enum time_constants {
    PLAYER_REGEN_INTERVAL = 20,
    SHIELD_CHIP_INTERVAL = 2,
    SHIELD_BREAK_INTERVAL = 10
};

/*! \brief Caps on statistics. */
enum player_stat_consts
{
    MAX_EXP_LEVEL = 99,
    BASE_PLAYER_HP = 20,
    PLAYER_HP_PER_LVL = 1,
    MAX_PLAYER_HP = 999,
    BASE_PLAYER_SH = 30,
    PLAYER_SH_PER_LVL = 5,
    MAX_PLAYER_SH = 999,
    BASE_PLAYER_TOHIT = 0,
    PLAYER_TOHIT_PER_LVL = 2,
    BASE_PLAYER_EVASION = 0,
    PLAYER_EVA_PER_LVL = 2,
    BASE_PLAYER_SHIELD_REGEN = 1,
    MAX_PLAYER_SHIELD_REGEN = 999
};

/*! \brief Out-of-range values for inventory slot designation */
enum special_slot_nums
{
    NO_SLOT = -1,
    INVALID_SLOT = -2
};

/*! \brief Numeric representation of terrain */
typedef enum terrain_num {
    WALL = 0, FLOOR = 1, DOOR = 2, STAIRS = 3
} Terrain;

/*! \brief Action speed of an actor */
typedef enum speed {
    Speed_slow = 0, /*!< 66% of normal speed */
    Speed_normal = 1,
    Speed_fast = 2 /*!< 133% of normal speed */
} Speed;

/*! \brief AI pathing algorithm to use */
typedef enum ai_style
{
    AI_charger = 0, //!< Closes as directly as possible.
    AI_seek_cardinal = 1, //!< Tries to get onto cardinals 
    AI_avoid_cardinal = 2, //!< Tries to stay off cardinals while closing
    AI_supine = 3, //!< Does not move
    AI_patrol = 4, //!< Follows a fixed route
    AI_escaper = 5, //!< flees from enemies
} AI_style;

/*! \brief Actor faction alignment */
typedef enum faction
{
    Faction_neutral = 0, //!< fears/hates whoever upsets them first
    Faction_friendly = 1, //!< Aligned with the player - or is the player!
    Faction_hostile = 2, //!< fears/hates the player
    Faction_betrayed = 3 //!< former friendly/neutral hurt by player
} Faction;

/*! \brief "Rogue value" for x/y coordinates */
#define NO_POS (INT_MIN)

/*! \brief Enumeration of commands used by the game */
typedef enum game_cmd {
    MOVE_WEST, MOVE_SOUTH, MOVE_NORTH, MOVE_EAST, MOVE_NW, MOVE_NE, MOVE_SW,
    MOVE_SE, GO_DOWN_STAIRS, STAND_STILL,
    GET_ITEM, DROP_ITEM, USE_CONSUMABLE,
    WIELD_WEAPON, UNWIELD_MELEE, UNWIELD_RANGED,
    WEAR_ARMOUR, TAKE_OFF_ARMOUR,
    PLUG_IN_PLUGIN, REMOVE_PLUGIN,
    ATTACK,
    QUIT, SAVE_GAME
} Game_cmd;

/*! \brief Enumeration for designating success/failure */
typedef enum passfail
{
    PF_pass = 0,
    PF_fail = -1,
} Passfail;

/*! \brief Article specifier for pretty-printing names */
typedef enum article
{
    Art_indef,
    Art_def,
    Art_indef_cap,
    Art_def_cap
} Article;

enum display_geometry
{
    CAMERA_SIZE = 21,
    MINICAM_SIZE = 5,
    MIN_SUPPORTED_LINES = 24,
    MIN_SUPPORTED_COLS = 80,
    BIG_SCREEN_LINES = 46
};

#define DEFAULT_PLAYER_NAME "MQ423"
#define GAME_TITLE "Phase Correspondence"

/* consts.h */
