/*! \file ai.c
 * \brief monster AI functionality
 */

/* Copyright © 2005, 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "central.h"
#include "monfun.h"
#include "mapfun.h"

/*! \brief Pointer to function for deciding where a monster should move to
 *
 * These functions return false if there is no valid better square.
 */
typedef bool (*Selector_func)(int mon, int * restrict py, int * restrict px, int goaly, int goalx);

#ifdef CHECK_GEOMETRY_ERRORS
/*! \brief Recurring pattern: bug alert for unexpected pts_ret value. */
#define PTSRET_CHECK(fun,expected) do {\
    if (pts_ret != (expected))\
    {\
        BUG("%s returned %d (expected %d)\n", (# fun), pts_ret, expected); \
        return false; \
    } \
} while (0)
#else
#define PTSRET_CHECK(fun,expected) ((void) 0)
#endif

/*! \brief Recurring pattern: call a geometry function to get some points */
#define GET_POINTS(fun,num) do {\
    pts_ret = (fun)(dy, dx, ndy, ndx, (num));\
    PTSRET_CHECK(fun,(num));\
} while (0)

/*! \brief Recurring pattern: if entry i is passable, return it. */
#define SELPATT_BASIC(i) do { \
    if ((!pos_is_nowhere(ndy[(i)], ndx[(i)])) && \
        mon_can_pass(mon, *py + ndy[(i)], *px + ndx[(i)])) \
    {\
        *py += ndy[(i)];\
        *px += ndx[(i)];\
        return true;\
    }\
} while (0)

/*! \brief Recurring pattern: conditionally coinflip
 * 
 * If a certain condition is true *or* a coinflip is true, then prefer entry
 * i over entry j. Otherwise, prefer entry j over entry i. */
#define SELPATT_COIN_OR(cond,i,j) do {\
    if ((cond) || coinflip())\
    {\
        SELPATT_BASIC((i));\
        SELPATT_BASIC((j));\
    }\
    else\
    {\
        SELPATT_BASIC((j));\
        SELPATT_BASIC((i));\
    }\
} while (0)

/*! \brief Recurring pattern: local vars for selector functions */
#define SELECTOR_VARS(max_pts) int dy = goaly - *py; \
int dx = goalx - *px;\
int ndy[(max_pts)];\
int ndx[(max_pts)];\
int pts_ret MYUNUSED

/*! \brief Recurring pattern: local vars for ortho/dia/range */
#define SPACE_VARS() \
    int ortho_dist MYUNUSED; \
    int dia_dist MYUNUSED; \
    int range MYUNUSED;

#define INIT_SPACE_VARS() do \
{\
    ortho_dist = geom_dist_from_orthogonal(dy, dx); \
    dia_dist = geom_dist_from_diagonal(dy, dx); \
    range = cheby_magnitude(dy, dx); \
} while (0)

/*! \brief Recurring pattern: initialize the potential deltas as nowhere */
#define INIT_NEW_POINTS(max_pts) do \
{\
    int i;\
    for (i = 0; i < (max_pts); ++i)\
    {\
        ndy[i] = ndx[i] = NO_POS;\
    }\
} while (0)

/*! \brief select a destination for a "charger" AI */
static bool select_for_charger(int mon, int * restrict py, int * restrict px, int goaly, int goalx)
{
    SELECTOR_VARS(3);
    INIT_NEW_POINTS(3);
    GET_POINTS(geom_towards_goal, 3);
    SELPATT_BASIC(0);
    SELPATT_COIN_OR((myabs(dy) != myabs(dx)), 1, 2);
    return false;
}

/*! \brief Select a destination for a cardinal-seeking AI */
static bool select_for_card_seeker(int mon, int *py, int *px, int goaly, int goalx)
{
    SELECTOR_VARS(3);
    SPACE_VARS();
    INIT_NEW_POINTS(3);
    INIT_SPACE_VARS();
    if (geom_is_cardinal(dy, dx))
    {
        GET_POINTS(geom_towards_goal, 1);
        SELPATT_BASIC(0);
        return false;
    }
    if (range < 3)
    {
        GET_POINTS(geom_towards_goal, 3);
        SELPATT_BASIC(0);
        SELPATT_COIN_OR(true, 1, 2);
        return false;
    }
    if (dia_dist >= ortho_dist)
    {
        GET_POINTS(geom_towards_orthogonal, 3);
        SELPATT_BASIC(0);
        SELPATT_COIN_OR(true, 1, 2);
        return false;
    }
    GET_POINTS(geom_towards_diagonal, 3);
    SELPATT_BASIC(0);
    SELPATT_COIN_OR(true, 1, 2);
    return false;
}

/*! \brief Select a destination for a cardinal-avoiding AI */
static bool select_for_card_avoider(int mon, int *py, int *px, int goaly, int goalx)
{
    SELECTOR_VARS(3);
    SPACE_VARS();
    INIT_SPACE_VARS();
    INIT_NEW_POINTS(3);
    /* Usefully distinct cases:
     * 
     * At range < 3 - close with goal
     */
    if (range < 3)
    {
        GET_POINTS(geom_towards_goal, 1);
        SELPATT_BASIC(0);
        return false;
    }
    return false;
}

/*! \brief AI "selector" for the supine AI, which doesn't move. */
static bool select_for_supine(int mon MYUNUSED, int *py MYUNUSED, int *px MYUNUSED, int goaly MYUNUSED, int goalx MYUNUSED)
{
    return false;
}

/*! \brief AI selector for the patrol AI, which doesn't work yet. */
static bool select_for_patrol(int mon MYUNUSED, int *py MYUNUSED, int *px MYUNUSED, int goaly MYUNUSED, int goalx MYUNUSED)
{
    return false;
}

/*! \brief try to flee from the goal */
static bool select_for_escaper(int mon, int *py, int *px, int goaly, int goalx)
{
    SELECTOR_VARS(3);
    INIT_NEW_POINTS(3);
    GET_POINTS(geom_towards_goal, 3);
    SELPATT_BASIC(0);
    SELPATT_COIN_OR((myabs(dy) != myabs(dx)), 1, 2);
    return false;
}

static Selector_func selector_funcs[] = {
    select_for_charger,
    select_for_card_seeker,
    select_for_card_avoider,
    select_for_supine,
    select_for_patrol,
    select_for_escaper
};

void select_space(int * restrict py, int * restrict px, int goaly, int goalx, AI_style style)
{
    int y = *py;
    int x = *px;
    int mon = MON_AT(y, x);
    if (selector_funcs[style](mon, &y, &x, goaly, goalx))
    {
        *py = y;
        *px = x;
    }
    return;
}

/* ai.c */
