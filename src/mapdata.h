/*! \file mapdata.h
 * \brief map data header file for Phase Correspondence
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

typedef struct roombound {
    int32_t top_y;
    int32_t bot_y;
    int32_t left_x;
    int32_t right_x;
} Roombound;

enum map_geometry
{
    MAP_SIZE = 42, /*! \brief x/y dimension of level */
    ROOMS_PER_ROW = 3,
    ROOMS_PER_COLUMN = 3,
    MAX_ROOMS = 9
};

/*! \brief Mapcell state flags */
enum mapflags {
    MAPFLAG_EXPLORED = 0x00000001u /*!< this square has been explored */
};

/*! \brief not in a room */
#define NO_ROOM (-1)

/*! \brief nature of linkage between rooms */
typedef enum roomlinkage {
    Roomlink_none = 0, /*!< rooms not linked */
    Roomlink_direct, /*!< rooms directly connected to each other */
    Roomlink_indirect, /*!< rooms indirectly connected to each other */
    Roomlink_self /*!< room is connected to itself because it is itself */
} Roomlinkage;

enum
{
    MAP_LAYERS = 2
};

enum labelled_layer
{
    Layer_ortho = 0,
    Layer_para = 1
};

typedef struct map_layer {
    int obj[MAP_SIZE][MAP_SIZE];
    int mon[MAP_SIZE][MAP_SIZE];
    Terrain terrain[MAP_SIZE][MAP_SIZE];
    uint32_t flag[MAP_SIZE][MAP_SIZE];
    int8_t region[MAP_SIZE][MAP_SIZE];
} Layer;

typedef struct game_map {
    Layer layers[MAP_LAYERS];
} Map;

Map *current_map;

GLOBALDATA Roombound roombounds[MAX_ROOMS];
GLOBALDATA int32_t dun_max_y;
GLOBALDATA int32_t dun_max_x;
GLOBALDATA int32_t dun_min_y;
GLOBALDATA int32_t dun_min_x;
GLOBALDATA int32_t floor_obj_pool;
GLOBALDATA int32_t floor_mon_pool;

/* mapdata.h */
