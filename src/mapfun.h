/*! \file mapfun.h
 * \brief map functions header file
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#ifndef INC_mapfun_h
#define INC_mapfun_h

#include "central.h"

GLOBALFUNC void leave_level(int depth);
GLOBALFUNC void make_new_level(int depth);
GLOBALFUNC void build_level(int depth);
GLOBALFUNC void populate_level(int depth);
GLOBALFUNC void inject_player(int depth);
GLOBALFUNC int get_room_x(int room);
GLOBALFUNC int get_room_y(int room);
GLOBALFUNC void room_reset(int8_t map_num);

/*! \brief Which row of the room grid is a room on? */
INLINEFUNC int room_row(int r)
{
    return r / ROOMS_PER_ROW;
}

/*! \brief Which column of the room grid is a room on? */
INLINEFUNC int room_column(int r)
{
    return r % ROOMS_PER_ROW;
}

/*! \brief Overwrite the map flags at a point in space */
INLINEFUNC void overwrite_map_flags_at(int8_t map MYUNUSED, int8_t layer, int8_t y, int8_t x, uint32_t bits)
{
    current_map->layers[layer].flag[y][x] = bits;
}

/*! \brief Change some map flags from 1 to 0 at a point in space */
INLINEFUNC void clr_map_flags_at(int8_t map MYUNUSED, int8_t layer, int8_t y, int8_t x, uint32_t bits)
{
    current_map->layers[layer].flag[y][x] &= ~bits;
}

/*! \brief Change some map flags from 0 to 1 at a point in space */
INLINEFUNC void set_map_flags_at(int8_t map MYUNUSED, int8_t layer, int8_t y, int8_t x, uint32_t bits)
{
    current_map->layers[layer].flag[y][x] |= bits;
}

/*! \brief Set the region number at a point in space */
INLINEFUNC void set_region_at(int8_t map MYUNUSED, int8_t layer, int8_t y, int8_t x, int8_t rnum)
{
    current_map->layers[layer].region[y][x] = rnum;
}

/*! \brief Set the terrain at a point in space */
INLINEFUNC void set_terrain_at(int8_t map MYUNUSED, int8_t layer, int8_t y, int8_t x, int8_t terr)
{
    current_map->layers[layer].terrain[y][x] = terr;
}

/*! \brief Set the monster at a point in space */
INLINEFUNC void set_mon_at(int8_t map MYUNUSED, int8_t layer, int8_t y, int8_t x, int32_t mon)
{
    current_map->layers[layer].mon[y][x] = mon;
}

/*! \brief Set the object at a point in space */
INLINEFUNC void set_obj_at(int8_t map MYUNUSED, int8_t layer, int8_t y, int8_t x, int32_t obj)
{
    current_map->layers[layer].obj[y][x] = obj;
}

#define MON_AT(y,x) (current_map->layers[u.layer].mon[(y)][(x)])
#define OBJ_AT(y,x) (current_map->layers[u.layer].obj[(y)][(x)])
#define TERR_AT(y,x) (current_map->layers[u.layer].terrain[(y)][(x)])
#define FLAGS_AT(y,x) (current_map->layers[u.layer].flag[(y)][(x)])
#define REGION_AT(y,x) (current_map->layers[u.layer].region[(y)][(x)])

#define SET_MON_AT(y,x,m) set_mon_at(u.map_num, u.layer, (y), (x), (m))
#define SET_OBJ_AT(y,x,o) set_obj_at(u.map_num, u.layer, (y), (x), (o))
#define SET_TERR_AT(y,x,t) set_terrain_at(u.map_num, u.layer, (y), (x), (t))
#define SET_FLAGS_AT(y,x,f) set_map_flags_at(u.map_num, u.layer, (y), (x), (f))
#define CLR_FLAGS_AT(y,x,f) clr_map_flags_at(u.map_num, u.layer, (y), (x), (f))
#define OVERWRITE_FLAGS_AT(y,x,f) overwrite_map_flags_at(u.map_num, u.layer, (y), (x), (f))
#define SET_REGION_AT(y,x,r) (current_map->layers[0].region[(y)][(x)] = (r))

#define FLOOR_OBJ(n) POOLED_OBJ(floor_obj_pool, (n))
#define FLOOR_OBJ(n) POOLED_OBJ(floor_obj_pool, (n))
#define FLOOR_MON(n) POOLED_MON(floor_mon_pool, (n))

#endif

/* mapfun.h */
