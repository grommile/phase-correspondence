All contributions are accepted on the same licence terms as the game is
distributed under. If this dissatisfies you, feel free to fork instead (the
game is under a permissive licence, after all).

