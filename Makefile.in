# Makefile skeleton for Phase Correspondence

# Copyright © 2016 Martin Read

# Pick up the configuration variables
include dirs.mk
include features.mk
include colophon.mk
include version.mk

FEATURE_DEFS::=
DISPLAY_LIBS::=
DISPLAY_INCPATHS::=
OTHER_LIBS::=$(shell pkg-config --libs libxdg-basedir)

ifeq ($(enable_compressed_saves),yes)
FEATURE_DEFS+=-DCOMPRESSED_SAVES 
endif
ifeq ($(enable_ncursesw),yes)
FEATURE_DEFS+=-DDISPLAY_NCURSESW 
DISPLAY_DEFS+=$(shell pkg-config --cflags-only-other panelw ncursesw) 
DISPLAY_LIBS+=$(shell pkg-config --libs panelw ncursesw) 
DISPLAY_INCS+=$(shell pkg-config --cflags-only-I panelw ncursesw) 
endif
ifeq ($(enable_sfml),yes)
FEATURE_DEFS+=-DDISPLAY_SFML 
DISPLAY_LIBS+=-lsfml-graphics -lsfml-window -lsfml-system 
endif
DIRDEFS::=-DDATADIR=$(datadir)
vpath %.o .
vpath %.h $(srcdir)/src

OBJS::=u.o permobj.o permons.o display.o main.o map.o objects.o monsters.o combat.o obj2.o prng.o ai.o geometry.o serdes.o dice.o util.o runtimecfg.o
GAME::=phasecorr
INCS::=$(DISPLAY_INCS) -I$(srcdir)/src
TRUE_CFLAGS::=$(CFLAGS) -g -O2 -Wall -Wextra -Winit-self -Wformat=2 -Wstrict-prototypes -Wwrite-strings -std=gnu11 -Wmissing-prototypes -Werror -fPIE -fpie -fstack-protector-strong -D_FORTIFY_SOURCE=2 $(FEATURE_DEFS) $(DISPLAY_DEFS) $(INCS)
LINKFLAGS::=-std=gnu11 -fPIE -fpie -fstack-protector-strong -O2 -g -Wl,-z,relro

.PHONY: all archive clean install

all: $(GAME)

$(GAME): $(OBJS)
	$(CXX) $(LINKFLAGS) $(OBJS) $(DISPLAY_LIBS) $(OTHER_LIBS) -o $(GAME)

install: all
	install --mode=0755 -D $(GAME) $(DESTDIR)$(gamesdir)/$(GAME)

archive:
	mkdir -p archivebuild/$(GAME)-1.0
	cp -R Makefile README.md LICENSE src archivebuild/$(GAME)-1.0
	(cd archivebuild && tar cvzf $(GAME)_1.0.orig.tar.gz $(GAME)-1.0)

clean:
	-rm -f *.o $(GAME)

%.o: $(srcdir)/src/%.c
	$(CC) $(TRUE_CFLAGS) -c $< -o $@

%.o: $(srcdir)/src/%.cc
	$(CC) $(TRUE_CXXFLAGS) -c $< -o $@

ai.o: $(srcdir)/src/ai.c central.h monfun.h Makefile

combat.o: $(srcdir)/src/combat.c central.h objfun.h pobjfun.h monfun.h pmonfun.h ufun.h objdata.h permobjdata.h mondata.h permondata.h Makefile consts.h

dice.o: $(srcdir)/src/dice.c central.h config.h prng.h consts.h Makefile

display.o: $(srcdir)/src/display.c central.h monfun.h objfun.h pobjfun.h playerdata.h mapdata.h objdata.h permobjdata.h permondata.h mondata.h ufun.h Makefile consts.h

geometry.o: $(srcdir)/src/geometry.c central.h Makefile consts.h

main.o: $(srcdir)/src/main.c central.h objfun.h monfun.h objfun.h pobjfun.h ufun.h playerdata.h Makefile consts.h config.h

map.o: $(srcdir)/src/map.c central.h objfun.h monfun.h objfun.h pobjfun.h mapfun.h ufun.h mapdata.h permobjdata.h objdata.h mondata.h Makefile consts.h permondata.h

monsters.o: $(srcdir)/src/monsters.c central.h objfun.h pobjfun.h monfun.h pmonfun.h ufun.h Makefile consts.h mondata.h permondata.h

obj2.o: $(srcdir)/src/obj2.c central.h objfun.h objfun.h pobjfun.h ufun.h Makefile consts.h permobjdata.h objdata.h

objects.o: $(srcdir)/src/objects.c central.h objfun.h monfun.h pmonfun.h pobjfun.h ufun.h Makefile consts.h permobjdata.h objdata.h

permobj.o: $(srcdir)/src/permobj.c central.h Makefile consts.h permobjdata.h

permons.o: $(srcdir)/src/permons.c central.h Makefile consts.h permondata.h

prng.o: $(srcdir)/src/prng.c prng.h Makefile

serdes.o: $(srcdir)/src/serdes.c central.h permondata.h permobjdata.h mapdata.h objdata.h mondata.h playerdata.h Makefile consts.h config.h

u.o: $(srcdir)/src/u.c central.h ufun.h objfun.h playerdata.h objdata.h Makefile consts.h

util.o: $(srcdir)/src/util.c util.h Makefile

